<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en">
<!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include_once'meta.php';?>

    <script src="assets/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="assets/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="assets/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="assets/view/theme/Frruit/stylesheet/stylesheet.css" rel="stylesheet" />

    <!-- Codezeel www.codezeel.com - Start -->
    <link rel="stylesheet" type="text/css" href="assets/view/javascript/jquery/magnific/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/custom.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/lightbox.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/animate.css" />



    <link href="assets/view/javascript/jquery/swiper/css/swiper.min.css" type="text/css" rel="stylesheet" media="screen" />
    <link href="assets/view/javascript/jquery/swiper/css/opencart.css" type="text/css" rel="stylesheet" media="screen" />
    <script src="assets/view/javascript/jquery/swiper/js/swiper.jquery.js" type="text/javascript"></script>


    <script type="text/javascript" src="assets/view/javascript/codezeel/custom.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jstree.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/carousel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/codezeel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.custom.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.formalize.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/lightbox/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/tabs.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.elevatezoom.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/doubletaptogo.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/parallax.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>

    <script src="assets/view/javascript/common.js" type="text/javascript"></script>
</head>


<body class="product-category-24 layout-2 left-col">
     <nav id="top">
        <?php include_once'top_header.php';?>
    </nav>


    <header>
        <?php include_once'header.php';?>
       
    </header>

<div class="container">
        <div class="row category_thumb">
            <div class="col-sm-2 category_img"><img src="image/banner/banner2.png" alt="Citrus Frruit" title="Citrus Frruit" class="img-responsive"></div>

        </div>

    </div>
    <div class="wrap-breadcrumb parallax-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                <li>Recipes</li>


            </ul>
        </div>
    </div>

    <!-- ======= Quick view JS ========= -->
    <script>
        function quickbox() {
            if ($(window).width() > 767) {
                $('.quickview-button').magnificPopup({
                    type: 'iframe',
                    delegate: 'a',
                    preloader: true,
                    tLoading: 'Loading image #%curr%...',
                });
            }
        }
        jQuery(document).ready(function() {
            quickbox();
        });
        jQuery(window).resize(function() {
            quickbox();
        });

    </script>
    <div id="product-category" class="container">
        <ul class="breadcrumb">
        </ul>
        <div class="row">
            <aside id="column-left" class="col-sm-3 hidden-xs">
                <div class="box">
                    <div class="box-heading Categories">Categories</div>
                    <div class="box-content">
                        <ul class="box-category treeview-list treeview">
                            <li>
                                <a href="#">Frruit</a>
                            </li>
                            <li>
                                <a href="#">Citrus Frruit</a>
                            </li>
                            <li>
                                <a href="#">Orange</a>
                            </li>
                            <li>
                                <a href="#">Tropical Frruit</a>
                            </li>
                            <li>
                                <a href="#">Drupes Frruit</a>
                            </li>
                            <li>
                                <a href="#" class="active">Grape Frruit </a>
                            </li>
                            <li>
                                <a href="#">Ugli Frruit</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="box sidebarFilter">
                    <div class="box-heading">Refine Search</div>
                    <div class="filterbox">
                        <div class="list-group-filter">

                            <a class="list-group-item group-name">Color</a>
                            <div class="list-group-item">
                                <div id="filter-group1">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="4" />
                                            Green
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="2" />
                                            Red
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="3" />
                                            Yellow
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="1" />
                                            Blue
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <a class="list-group-item group-name">Size</a>
                            <div class="list-group-item">
                                <div id="filter-group2">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="5" />
                                            Small
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="6" />
                                            Medium
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="7" />
                                            Large
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <a class="list-group-item group-name">Weight</a>
                            <div class="list-group-item">
                                <div id="filter-group3">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="9" />
                                            5
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="10" />
                                            10
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="11" />
                                            15
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-right">
                            <button type="button" id="button-filter" class="btn btn-primary">Refine Search</button>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    <!--
                    $('#button-filter').on('click', function() {
                        filter = [];

                        $('input[name^=\'filter\']:checked').each(function(element) {
                            filter.push(this.value);
                        });

                        location = '' + filter.join(',');
                    });
                    //-->

                </script>


                <script type="text/javascript">
                    <!--
                    $('#banner0').swiper({
                        effect: 'fade',
                        autoplay: 2500,
                        pagination: '.swiper-pagination', // If we need pagination
                        autoplayDisableOnInteraction: false
                    });
                    -->

                </script>



            </aside>

            <div id="content" class="col-sm-9">
                <h1 class="page-title">Recipes</h1>

                <div class="category_filter">
                    <div class="col-md-4 btn-list-grid">
                        <div class="btn-group">
                            <button type="button" id="grid-view" class="btn btn-default grid" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                            <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                        </div>
                    </div>
                    <div class="compare-total"><a href="#" id="compare-total"> Items 1-9 of 19</a></div>
                    <div class="pagination-right">
                        <div class="sort-by-wrapper">
                            <div class="col-md-2 text-right sort-by">
                                <label class="control-label" for="input-sort">Sort By:</label>
                            </div>
                            <div class="col-md-3 text-right sort">
                                <select id="input-sort" class="form-control" onchange="location = this.value;">

                                    <option value="" selected="selected">Default</option>

                                    <option value="">Name (A - Z) </option>

                                    <option value="">Name (Z - A) </option>

                                    <option value="">Price (Low &gt; High) </option>

                                    <option value="">Price (High &gt; Low) </option>

                                    <option value="">Rating (Highest) </option>

                                    <option value="">Rating (Lowest) </option>

                                    <option value="">Model (A - Z) </option>

                                    <option value="">Model (Z - A) </option>
                                </select>
                            </div>
                        </div>
                        <div class="show-wrapper">
                            <div class="col-md-1 text-right show">
                                <label class="control-label" for="input-limit">Show:</label>
                            </div>
                            <div class="col-md-2 text-right limit">
                                <select id="input-limit" class="form-control" onchange="location = this.value;">
                                    <option value="" selected="selected">9</option>
                                    <option value="">25 </option>
                                    <option value="">50 </option>
                                    <option value="">75 </option>
                                    <option value="">100 </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row cat_prod">
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition ">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="recipe-description.php">
                                        <img src="image/recipe/recipe3.jpg" title="Butter Chicken with Rice

" alt="Butter Chicken with Rice

" class="img-responsive list-img" />

                                    </a>


                                    <span class="saleicon sale">Sale</span>


                                    <!--    <div class="percentsaving">5% off</div>-->

                                    <div class="button-group">
                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('40 ');"><i class="fa fa-heart"></i></button>
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                            <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('40 ');"><i class="fa fa-exchange"></i></button>
                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('40 ');"><span>Add to Cart</span> </button>
                                    </div>
                                </div>


                            </div>

                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="recipe-description.php">Butter Chicken with Rice</a></h4>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="recipe-description.php">
                                        <img src="image/recipe/recipe3.jpg" title="Crispy Spring Rolls" alt="Crispy Spring Rolls" class="img-responsive list-img" />

                                    </a>


                                    <span class="saleicon sale">Sale</span>


                                    <!-- <div class="percentsaving">8% off</div>-->

                                    <div class="button-group">
                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('42 ');"><i class="fa fa-heart"></i></button>
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                            <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('42 ');"><i class="fa fa-exchange"></i></button>
                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('42 ');"><span>Add to Cart</span> </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="recipe-description.php">Crispy Spring Rolls </a></h4>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="recipe-description.php">
                                        <img src="image/recipe/recipe3.jpg" title="Curry Chicken Roti Paratha" alt="Curry Chicken Roti Paratha" class="img-responsive list-img" />

                                    </a>


                                    <span class="saleicon sale">Sale</span>


                                    <!--   <div class="percentsaving">11% off</div>-->

                                    <div class="button-group">
                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('30 ');"><i class="fa fa-heart"></i></button>
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                            <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('30 ');"><i class="fa fa-exchange"></i></button>
                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('30 ');"><span>Add to Cart</span> </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="recipe-description.php">Curry Chicken Roti Paratha</a></h4>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="recipe-description.php">
                                        <img src="image/recipe/recipe3.jpg" title="Dutch Pea and Ham Soup" alt="Dutch Pea and Ham Soup" class="img-responsive list-img" />

                                    </a>
                                    <span class="saleicon sale">Sale</span>


                                    <div class="button-group">
                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('43 ');"><i class="fa fa-heart"></i></button>
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                            <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('43 ');"><i class="fa fa-exchange"></i></button>
                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('43 ');"><span>Add to Cart</span> </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="recipe-description.php">Dutch Pea and Ham Soup</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="recipe-description.php">
                                        <img src="image/recipe/recipe3.jpg" title="Pork Loin with Sweet and Savoury Fig Sauce" alt="Pork Loin with Sweet and Savoury Fig Sauce" class="img-responsive list-img" />

                                    </a>
                                    <span class="saleicon sale">Sale</span>


                                    <div class="button-group">
                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('43 ');"><i class="fa fa-heart"></i></button>
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                            <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('43 ');"><i class="fa fa-exchange"></i></button>
                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('43 ');"><span>Add to Cart</span> </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="recipe-description.php">Pork Loin with Sweet and Savoury Fig Sauce</a></h4>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="recipe-description.php">
                                        <img src="image/recipe/recipe3.jpg" title="Pumpkin, Potato and Zucchini Soup" alt="Pumpkin, Potato and Zucchini Soup" class="img-responsive list-img" />

                                    </a>
                                    <span class="saleicon sale">Sale</span>


                                    <div class="button-group">
                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('43 ');"><i class="fa fa-heart"></i></button>
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                            <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('43 ');"><i class="fa fa-exchange"></i></button>
                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('43 ');"><span>Add to Cart</span> </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="recipe-description.php">Pumpkin, Potato and Zucchini Soup</a></h4>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="recipe-description.php">
                                        <img src="image/recipe/recipe3.jpg" title="Shredded Chicken & Bamboo Shoot Wraps" alt="Shredded Chicken & Bamboo Shoot Wraps" class="img-responsive list-img" />

                                    </a>
                                    <span class="saleicon sale">Sale</span>


                                    <div class="button-group">
                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('43 ');"><i class="fa fa-heart"></i></button>
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                            <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('43 ');"><i class="fa fa-exchange"></i></button>
                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('43 ');"><span>Add to Cart</span> </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="recipe-description.php">Shredded Chicken & Bamboo Shoot Wraps</a></h4>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="recipe-description.php">
                                        <img src="image/recipe/recipe3.jpg" title="Pumpkin, Potato and Zucchini Soup" alt="Pumpkin, Potato and Zucchini Soup" class="img-responsive list-img" />

                                    </a>
                                    <span class="saleicon sale">Sale</span>


                                    <div class="button-group">
                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('43 ');"><i class="fa fa-heart"></i></button>
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                            <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('43 ');"><i class="fa fa-exchange"></i></button>
                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('43 ');"><span>Add to Cart</span> </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="recipe-description.php">Shredded Chicken & Bamboo Shoot Wraps</a></h4>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="recipe-description.php">
                                        <img src="image/recipe/recipe3.jpg" title="Spam and Noodles" alt="Spam and Noodles" class="img-responsive list-img" />

                                    </a>
                                    <span class="saleicon sale">Sale</span>


                                    <div class="button-group">
                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('43 ');"><i class="fa fa-heart"></i></button>
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                            <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('43 ');"><i class="fa fa-exchange"></i></button>
                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('43 ');"><span>Add to Cart</span> </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="recipe-description.php">Spam and Noodles</a></h4>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="recipe-description.php">
                                        <img src="image/recipe/recipe3.jpg" title="Tropical Crepes Millefeuille" alt="Tropical Crepes Millefeuille" class="img-responsive list-img" />

                                    </a>
                                    <span class="saleicon sale">Sale</span>


                                    <div class="button-group">
                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('43 ');"><i class="fa fa-heart"></i></button>
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                            <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('43 ');"><i class="fa fa-exchange"></i></button>
                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('43 ');"><span>Add to Cart</span> </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="recipe-description.php">Tropical Crepes Millefeuille</a></h4>

                            </div>
                        </div>
                    </div>



                </div>
                <div class="pagination-wrapper">
                    <div class="col-sm-6 text-left page-link">
                        <ul class="pagination">
                            <li class="active"><span>1</span></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">&gt;</a></li>
                            <li><a href="#">&gt;|</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 text-right page-result">Showing 1 to 9 of 11 (2 Pages)</div>
                </div>
            </div>
        </div>
    </div>
  <footer>
       <?php include_once'footer.php';?>
    </footer>

</body>

</html>
