<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en">
<!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include_once'meta.php';?>

    <script src="assets/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="assets/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="assets/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="assets/view/theme/Frruit/stylesheet/stylesheet.css" rel="stylesheet" />

    <!-- Codezeel www.codezeel.com - Start -->
    <link rel="stylesheet" type="text/css" href="assets/view/javascript/jquery/magnific/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/custom.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/lightbox.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/animate.css" />



    <link href="assets/view/javascript/jquery/swiper/css/swiper.min.css" type="text/css" rel="stylesheet" media="screen" />
    <link href="assets/view/javascript/jquery/swiper/css/opencart.css" type="text/css" rel="stylesheet" media="screen" />
    <script src="assets/view/javascript/jquery/swiper/js/swiper.jquery.js" type="text/javascript"></script>


    <script type="text/javascript" src="assets/view/javascript/codezeel/custom.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jstree.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/carousel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/codezeel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.custom.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.formalize.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/lightbox/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/tabs.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.elevatezoom.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/doubletaptogo.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/parallax.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>

    <script src="assets/view/javascript/common.js" type="text/javascript"></script>
</head>


<body class="product-category-24 layout-2 left-col">
    <nav id="top">
        <?php include_once'top_header.php';?>
    </nav>


    <header>
        <?php include_once'header.php';?>

    </header>
    <div class="container">
        <div class="row category_thumb">
            <div class="col-sm-2 category_img"><img class="img-responsive" src="image/banner/banner2.png" alt="Citrus Frruit" title="Citrus Frruit" class="img-responsive"></div>

        </div>

    </div>

    <div class="wrap-breadcrumb parallax-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                <li>Our Products</li>


            </ul>
        </div>
    </div>

    <!-- ======= Quick view JS ========= -->
    <script>

        function quickbox(){
 if ($(window).width() > 767) {
		$('.quickview-button').magnificPopup({
			type:'iframe',
			delegate: 'a',
			preloader: true,
			tLoading: 'Loading image #%curr%...',
		});
 }	
}
jQuery(document).ready(function() {quickbox();});
jQuery(window).resize(function() {quickbox();});

</script>
    <div id="product-category" class="container">
        <ul class="breadcrumb">
        </ul>
        <div class="row">
            <aside id="column-left" class="col-sm-3 hidden-xs">
                <div class="box">
                    <div class="box-heading Categories">Shop By</div>
                    <div class="box-content">
                        <ul class="box-category treeview-list treeview">
                            <li>
                                <a href="#" style="font-weight: 600;">BRAND</a>
                            </li>
                            <li>
                                <a href="#">Hong Australia</a>
                            </li>
                            <li>
                                <a href="#">Epic Cash & Carry</a>
                            </li>
                            <li>
                                <a href="#">Garboro Foods</a>
                            </li>
                            <li>
                                <a href="#">Merchant Australia</a>
                            </li>
                            <li>
                                <a href="#">Epic Cash & Carry</a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="box sidebarFilter">
                    <div class="box-heading">Refine Search</div>
                    <div class="filterbox">
                        <div class="list-group-filter">

                            <a class="list-group-item group-name">Color</a>
                            <div class="list-group-item">
                                <div id="filter-group1">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="4" />
                                            Green
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="2" />
                                            Red
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="3" />
                                            Yellow
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="filter[]" value="1" />
                                            Blue
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="box">
                                <div class="box-heading Categories">MANUFACTURER</div>
                                <div class="box-content">
                                    <ul class="box-category treeview-list treeview">

                                        <li>
                                            <a href="#">Germany</a>
                                        </li>
                                        <li>
                                            <a href="#">USA</a>
                                        </li>
                                        <li>
                                            <a href="#">China</a>
                                        </li>
                                        <li>
                                            <a href="#">France</a>
                                        </li>


                                    </ul>
                                </div>
                            </div>
                            <a class="list-group-item group-name">PACK SIZE</a>
                            <div class="box-content">
                                <ul class="box-category treeview-list treeview">

                                    <li>
                                        <a href="#">500gms</a>
                                    </li>
                                    <li>
                                        <a href="#">1kg</a>
                                    </li>

                                </ul>
                            </div>

                            <a class="list-group-item group-name">EVENT</a>
                            <div class="box-content">
                                <ul class="box-category treeview-list treeview">

                                    <li>
                                        <a href="#">Home Office/Study</a>
                                    </li>
                                    <li>
                                        <a href="#">Bed room</a>
                                    </li>
                                    <li>
                                        <a href="#">Living room</a>
                                    </li>
                                    <li>
                                        <a href="#">Dining room</a>
                                    </li>

                                </ul>
                            </div>
                            <a class="list-group-item group-name">STYLE</a>
                            <div class="box-content">
                                <ul class="box-category treeview-list treeview">

                                    <li>
                                        <a href="#">Artdeco</a>
                                    </li>
                                    <li>
                                        <a href="#">Modern</a>
                                    </li>
                                    <li>
                                        <a href="#">Haytech</a>
                                    </li>
                                    <li>
                                        <a href="#">Classic</a>
                                    </li>
                                    <li>
                                        <a href="#">Ecliptic</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="box">
                            <div class="box-heading Categories">Compare Products</div>
                            <div class="box-content">
                                <ul class="box-category treeview-list treeview">

                                    <li>
                                        <a href="#">You have no items to compare</a>
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <div class="box">
                            <div class="box-heading Categories">My Wishlist</div>

                        </div>
                        <div class="box">
                            <div class="box-heading Categories">LAST ADDED ITEMS</div>

                        </div>
                        <div class="panel-footer text-right">
                            <button type="button" id="button-filter" class="btn btn-primary">Refine Search</button>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    <!--
                    $('#button-filter').on('click', function() {
                        filter = [];

                        $('input[name^=\'filter\']:checked').each(function(element) {
                            filter.push(this.value);
                        });

                        location = '' + filter.join(',');
                    });
                    //-->

                </script>


                <script type="text/javascript">
                    <!--
                    $('#banner0').swiper({
                        effect: 'fade',
                        autoplay: 2500,
                        pagination: '.swiper-pagination', // If we need pagination
                        autoplayDisableOnInteraction: false
                    });
                    -->

                </script>



            </aside>

            <div id="content" class="col-sm-9">
                <h1 class="page-title">Our Products</h1>

                <div class="category_filter">
                    <div class="col-md-4 btn-list-grid">
                        <div class="btn-group">
                            <button type="button" id="grid-view" class="btn btn-default grid" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                            <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                        </div>
                    </div>
                    <div class="compare-total"><a href="#" id="compare-total"> Items 1-9 of 19</a></div>
                    <div class="pagination-right">
                        <div class="sort-by-wrapper">
                            <div class="col-md-2 text-right sort-by">
                                <label class="control-label" for="input-sort">Sort By:</label>
                            </div>
                            <div class="col-md-3 text-right sort">
                                <select id="input-sort" class="form-control" onchange="location = this.value;">

                                    <option value="" selected="selected">Default</option>

                                    <option value="">Name (A - Z) </option>

                                    <option value="">Name (Z - A) </option>

                                    <option value="">Price (Low &gt; High) </option>

                                    <option value="">Price (High &gt; Low) </option>

                                    <option value="">Rating (Highest) </option>

                                    <option value="">Rating (Lowest) </option>

                                    <option value="">Model (A - Z) </option>

                                    <option value="">Model (Z - A) </option>
                                </select>
                            </div>
                        </div>
                        <div class="show-wrapper">
                            <div class="col-md-1 text-right show">
                                <label class="control-label" for="input-limit">Show:</label>
                            </div>
                            <div class="col-md-2 text-right limit">
                                <select id="input-limit" class="form-control" onchange="location = this.value;">
                                    <option value="" selected="selected">9</option>
                                    <option value="">25 </option>
                                    <option value="">50 </option>
                                    <option value="">75 </option>
                                    <option value="">100 </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row cat_prod">
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition ">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="#">
                                        <img src="image/list/14.png" title="Alpen Birthday Candle 24's" alt="Alpen Birthday Candle 24's" class="img-responsive list-img" />

                                    </a>


                                    <span class="saleicon sale">Sale</span>


                                    <!--    <div class="percentsaving">5% off</div>-->

                                    <div class="button-group">
                                   
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view"> <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                       
                                        
                                    </div>
                                </div>


                            </div>

                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="#">Alpen Birthday Candle 24's </a></h4>
                                <p>Pack Size: 12 x 24's</p>
                                <p>Product Id: 410816</p>

                                <div class="rating">
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    1Review


                                </div>
                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="#">
                                        <img src="image/list/3.jpg" title="Alpen Tealight Candle" alt="Alpen Tealight Candle" class="img-responsive list-img" />

                                    </a>


                                    <span class="saleicon sale">Sale</span>


                                    <!-- <div class="percentsaving">8% off</div>-->

                                    <div class="button-group">
                                       
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view"> <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                        
                                        
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="#">Alpen Tealight Candle 4hr 50's </a></h4>
                                <p>Pack Size: 12 x 50's</p>
                                <p>Product Id: 415523</p>

                                <div class="rating">
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    2Reviews


                                </div>
                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="#">
                                        <img src="image/list/2.jpg" title="Alpen Parasol Cocktail" alt="Alpen Parasol Cocktail" class="img-responsive list-img" />

                                    </a>


                                    <span class="saleicon sale">Sale</span>


                                    <!--   <div class="percentsaving">11% off</div>-->

                                    <div class="button-group">
                                        
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view"> <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                     
                                       
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="#">Alpen Parasol Cocktail 100's </a></h4>
                                <p>Pack Size: 50 x 100's</p>
                                <p>Product Id: 850489</p>

                                <div class="rating">
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    1Review


                                </div>
                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="#">
                                        <img src="image/list/6.jpg" title="Alpen Toothpick Double End" alt="Alpen Toothpick Double End" class="img-responsive list-img" />

                                    </a>
                                    <span class="saleicon sale">Sale</span>


                                    <div class="button-group">
                                        
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view"> <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                        
                                        
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="#">Aroy-D Nata Decoco in syrup 450g</a></h4>
                                <p>Pack Size: 24 x 450g</p>
                                <p>Product Id: 320152</p>
                                <p>Available in NSW</p>

                                <div class="rating">
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    1Review

                                </div>
                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="#">
                                        <img src="image/list/5.jpg" title="Alpen Toothpick Double End" alt="Alpen Toothpick Double End" class="img-responsive list-img" />

                                    </a>
                                    <span class="saleicon sale">Sale</span>


                                    <div class="button-group">
                                        
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view"> <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                        
                                        
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="#">HAC Coconut Extract 560ml</a></h4>
                                <p>Pack Size: 24 x 560ml</p>
                                <p>Product Id: 24070</p>
                                <p>Available in NSW,QLD</p>

                                <div class="rating">
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    2Reviews

                                </div>
                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                            </div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="#">
                                        <img src="image/list/fa.jpg" title="Alpen Toothpick Double End" alt="Alpen Toothpick Double End" class="img-responsive list-img" />

                                    </a>
                                    <span class="saleicon sale">Sale</span>


                                    <div class="button-group">
                                       
                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view"> <a class="quickbox" href="#">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                       
                                       
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="product-details text-center">
                            <div class="caption">
                                <h4 class="title-blue"><a href="#">Allens Fantales 1kg</a></h4>
                                <p>Pack Size: 6 x 1kg</p>
                                <p>Product Id: 130016</p>
                                <p>Available in NSW</p>

                                <div class="rating">
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    1Review

                                </div>
                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                            </div>
                        </div>
                    </div>



                </div>
                <div class="pagination-wrapper">
                    <div class="col-sm-6 text-left page-link">
                        <ul class="pagination">
                            <li class="active"><span>1</span></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">&gt;</a></li>
                            <li><a href="#">&gt;|</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 text-right page-result">Showing 1 to 9 of 11 (2 Pages)</div>
                </div>
            </div>
        </div>
    </div>
    <footer>

        <?php include_once'footer.php';?>

    </footer>

</body>

</html>
