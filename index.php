<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en">
<!--<![endif]-->
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include_once'meta.php';?>
       	
    <script src="assets/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="assets/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="assets/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="assets/view/theme/Frruit/stylesheet/stylesheet.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="assets/view/javascript/jquery/magnific/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/custom.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/lightbox.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/animate.css" />




    <link href="assets/view/javascript/jquery/swiper/css/swiper.min.css" type="text/css" rel="stylesheet" media="screen" />
    <link href="assets/view/javascript/jquery/swiper/css/opencart.css" type="text/css" rel="stylesheet" media="screen" />
    <script src="assets/view/javascript/jquery/swiper/js/swiper.jquery.js" type="text/javascript"></script>

    <script type="text/javascript" src="assets/view/javascript/codezeel/custom.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jstree.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/carousel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/codezeel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.custom.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.formalize.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/lightbox/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/tabs.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.elevatezoom.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/doubletaptogo.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/parallax.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>

    <script src="assets/view/javascript/common.js" type="text/javascript"></script>
   
</head>


<body class="common-home layout-1">
    <nav id="top">
        <?php include_once'top_header.php';?>
    </nav>


    <header>
        <?php include_once'header.php';?>
       
    </header>



    <div class="wrap-breadcrumb parallax-breadcrumb">
        <div class="container"></div>
    </div>

    <!-- ======= Quick view JS ========= -->
    <script>
        function quickbox() {
            if ($(window).width() > 767) {
                $('.quickview-button').magnificPopup({
                    type: 'iframe',
                    delegate: 'a',
                    preloader: true,
                    tLoading: 'Loading image #%curr%...',
                });
            }
        }
        jQuery(document).ready(function() {
            quickbox();
        });
        jQuery(window).resize(function() {
            quickbox();
        });

    </script>
    <div class="content-top">

        <div class="main-slider">
            <div id="spinner"></div>
            <div class="swiper-viewport">
                <div id="slideshow0" class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide text-center">
                            <a href="#"><img src="image/banner/home.jpg" alt="Main Banner1" class="img-responsive" /></a>
                        </div>
                        <div class="swiper-slide text-center">
                            <a href="#"><img src="image/banner/home.jpg" alt="Main Banner2" class="img-responsive" /></a>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination slideshow0"></div>
                <div class="swiper-pager">
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            <!--
            $('#slideshow0').swiper({
                mode: 'horizontal',
                slidesPerView: 1,
                pagination: '.slideshow0',
                paginationClickable: true,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                spaceBetween: 0,
                autoplay: 5000,
                autoplayDisableOnInteraction: true,
                loop: true
            });
            -->

        </script>

        <script type="text/javascript">
            // Can also be used with $(document).ready()
            $(window).load(function() {
                $("#spinner").fadeOut("slow");
            });

        </script>


    </div>
    <div id="czservicecmsblock">
        <h1 class="h1 products-section-title text-uppercase text-center"><span>Hong Australia Corporation</span></h1>
        <div class="service_container">
            <div class="service-area">
                <div class="col-md-7 service-left-part">
                    <div class="service-bottom-inner">
                        <div class="service-third service1">
                            <!-- <div class="service-icon icon1"></div>-->
                            <div class="service-content">
                                <div class="service-description">
                                    <p>For over 50 years, Hong Australia Corporation Pty Ltd (HAC) has been supplying groceries to your local supermarkets and your favourite eateries across Australia.</p>
                                </div>
                            </div>
                        </div>
                        <div class="service-third service2">
                            <!-- <div class="service-icon icon2"></div>-->
                            <div class="service-content">
                                <div class="service-description">Over the years, HAC’s product range have expanded to over 10,000 products. That means more variety and choices for our customers.</div>
                            </div>
                        </div>
                        <div class="service-third service3">
                            <!-- <div class="service-icon icon3"></div>-->
                            <div class="service-content">
                                <div class="service-description">While Hong Australia stock many well-known international and national brands, we also offer customers our very own home brand products; Royles, HAC, and Epic are great value products, and are sourced only from manufacturers with high regards to food safety.</div>
                            </div>
                        </div>
                        <div class="service-third service3">
                            <!-- <div class="service-icon icon3"></div>-->
                            <div class="service-content">
                                <div class="service-description">Hong Australia Corporation strongly believes in providing high level of service to our customers by inspiring our employees.

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-map">
                        <div class="service-banner1">
                            <a href="#" class="service-anchor"><img class="img-responsive about-bb" src="image/banner/about2.png" alt="service-banner1"></a>
                        </div>
                        <h1 class="text-right">Our Delivery Network</h1>
                    </div>

                </div>
                <div class="col-md-5 service-right-part">

                    <!--  <div class="service-banner2"><a href="#" class="service-anchor"><img src="image/catalog/service-banner2.png" alt="service-banner2"></a></div>-->
                    <div class="service-banner1">
                        <a href="#" class="service-anchor"><img class="img-responsive about-b" src="image/banner/about.jpg" alt="service-banner1"></a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row home_row">
        <div class="box-head">
            <div class="box-heading text-center">New Products</div>
        </div>
        <div class="box-content">

            <div class="box-product product-carousel" id="featured-carousel" style="opacity: 1; display: block;">
                <div class="slider-wrapper-outer prod">
                    <div class="slider-wrapper" style="width:100%; display: block; transform: translate3d(0px, 0px, 0px); transition: all 200ms ease 0s;">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="slider-item first_item_cz">
                            <div class="product-block product-thumb transition" style="height: 237px;">
                                <div class="product-block-inner">
                                    <div class="image">
                                        <a href="#">
                                            <img src="image/list/1.png" title="Allied Mills Wheaten Cornflour" alt="Allied Mills Wheaten Cornflour" class="img-responsive list-img">

                                        </a>


                                        <span class="saleicon sale">Sale</span>


                                        <!--    <div class="percentsaving">5% off</div>-->

                                        <div class="button-group">
                                            <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add('40 ');" data-placement="left" data-original-title="Add to Wish List "><i class="fa fa-heart"></i></button>
                                            <div class="quickview-button" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick view">
                                                <a class="quickbox" href="#">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                            <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add('40 ');" data-placement="left" data-original-title="Add to Compare "><i class="fa fa-exchange"></i></button>
                                            <button type="button" class="addtocart" data-toggle="tooltip" title="" onclick="cart.add('40 ');" data-placement="left" data-original-title="Add to Cart "><span>Add to Cart</span> </button>
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="product-details text-center">
                                <div class="caption">
                                    <h4 class="title-blue"><a href="#">Allied Mills Wheaten Cornflour 12.5kg </a></h4>
                                    <p>Pack Size: 1 x 12.5kg</p>
                                    <p>Product Id: 350046</p>

                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>


                                    </div>
                                    <p>Available in: NSW</p>
                                    <!--  <a class="wishlist"><i class="fa fa-heart"></i></a>
                                                        <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>-->

                                    <!-- <p class="price">
                                                            <span class="price-new">$122.00</span> <span class="price-old">$128.00</span>
                                                            <span class="price-tax">Ex Tax: $100.00</span>
                                                        </p>-->




                                </div>
                            </div>
                        </div>
                        </div>
                         <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="slider-item">
                            <div class="product-block product-thumb transition" style="height: 237px;">
                                <div class="product-block-inner">
                                    <div class="image">
                                        <a href="#">
                                            <img src="image/list/2.png" title="Amacon Singapore Chicken Curry Paste" alt="Amacon Singapore Chicken Curry Paste" class="img-responsive list-img">

                                        </a>


                                        <span class="saleicon sale">Sale</span>


                                        <!-- <div class="percentsaving">8% off</div>-->

                                        <div class="button-group">
                                            <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add('42 ');" data-placement="left" data-original-title="Add to Wish List "><i class="fa fa-heart"></i></button>
                                            <div class="quickview-button" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick view">
                                                <a class="quickbox" href="#">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                            <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add('42 ');" data-placement="left" data-original-title="Add to Compare "><i class="fa fa-exchange"></i></button>
                                            <button type="button" class="addtocart" data-toggle="tooltip" title="" onclick="cart.add('42 ');" data-placement="left" data-original-title="Add to Cart "><span>Add to Cart</span> </button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="product-details text-center">
                                <div class="caption">
                                    <h4 class="title-blue"><a href="#">Amacon Singapore Chicken Curry Paste 340g </a></h4>
                                    <p>Pack Size: 24 x 340g</p>
                                    <p>Product Id: 674476</p>

                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>


                                    </div>
                                    <p>Available in: NSW</p>

                                </div>
                            </div>
                        </div>
                        </div>
                         <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="slider-item">
                            <div class="product-block product-thumb transition" style="height: 237px;">
                                <div class="product-block-inner">
                                    <div class="image">
                                        <a href="#">
                                            <img src="image/list/3.png" title="Amacon Singapore Fish Curry Paste" alt="Amacon Singapore Fish Curry Paste" class="img-responsive list-img">

                                        </a>


                                        <span class="saleicon sale">Sale</span>


                                        <!--   <div class="percentsaving">11% off</div>-->

                                        <div class="button-group">
                                            <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add('30 ');" data-placement="left" data-original-title="Add to Wish List "><i class="fa fa-heart"></i></button>
                                            <div class="quickview-button" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick view">
                                                <a class="quickbox" href="#">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                            <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add('30 ');" data-placement="left" data-original-title="Add to Compare "><i class="fa fa-exchange"></i></button>
                                            <button type="button" class="addtocart" data-toggle="tooltip" title="" onclick="cart.add('30 ');" data-placement="left" data-original-title="Add to Cart "><span>Add to Cart</span> </button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="product-details text-center">
                                <div class="caption">
                                    <h4 class="title-blue"><a href="#">Amacon Singapore Fish Curry Paste 340g </a></h4>
                                    <p>Pack Size: 24 x 340g</p>
                                    <p>Product Id: 674477</p>

                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>


                                    </div>
                                    <p>Available in: NSW</p>

                                </div>
                            </div>
                        </div>
                        </div>
                         <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="slider-item last_item_cz">
                            <div class="product-block product-thumb transition" style="height: 237px;">
                                <div class="product-block-inner">
                                    <div class="image">
                                        <a href="#">
                                            <img src="image/list/4.png" title="Auscrown Portable Gas Butane Stove" alt="Auscrown Portable Gas Butane Stove" class="img-responsive list-img">

                                        </a>
                                        <span class="saleicon sale">Sale</span>


                                        <div class="button-group">
                                            <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add('43 ');" data-placement="left" data-original-title="Add to Wish List "><i class="fa fa-heart"></i></button>
                                            <div class="quickview-button" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick view">
                                                <a class="quickbox" href="#">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                            <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add('43 ');" data-placement="left" data-original-title="Add to Compare "><i class="fa fa-exchange"></i></button>
                                            <button type="button" class="addtocart" data-toggle="tooltip" title="" onclick="cart.add('43 ');" data-placement="left" data-original-title="Add to Cart "><span>Add to Cart</span> </button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="product-details text-center">
                                <div class="caption">
                                    <h4 class="title-blue"><a href="#">Auscrown Portable Gas Butane Stove </a></h4>
                                    <p>Pack Size: 6 x 1ea</p>
                                    <p>Product Id: 571313</p>

                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>

                                    </div>
                                    <p>Available in: NSW</p>

                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
                </div>






            </div>
        </div>
    </div>
    <div class="row home_row">
        <div class="container">

            <!-- <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="recipes-img">
                        <img class="img-responsive" src="image/banner/recipe.png">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="recipes-img">
                        <img class="img-responsive" src="image/banner/recipe2.png">
                    </div>
                </div>
            </div>-->
            <div id="cztestimonialcmsblock" class="czparallax">
                <div class="parallax czparallax_1">
                    <div class="testimonial_container container">
                        <div class="box-heading">Recipes</div>
                        <div class="testimonial_wrapper">
                            <div class="testimonial-area">
                               <!-- <div class="customNavigation"><a class="btn prev cztestimonial_prev">&nbsp;</a> <a class="btn next cztestimonial_next">&nbsp;</a></div>-->
                                <ul id="testimonial-carousel" class="cz-carousel product_list">
                                    <li class="slider-item">
                                        <div class="testimonial-item">

                                            <div class="product_inner_cms">
                                               
                                                    <img class="img-responsive" src="image/recipe/recipe.png">
                                                <a href="recipe-list.html" class="recipe-text">View Recipes</a>
                                                <a href="recipe-list.html" class="">Curry Chicken Roti Paratha</a>
                                                
                                            </div>
                                        </div>
                                    </li>
                                    <li class="slider-item">
                                        <div class="testimonial-item">

                                            <div class="product_inner_cms">
                                               
                                                    <img class="img-responsive" src="image/recipe/recipe2.png">
                                                 <a href="recipe-list.html" class="recipe-text">View Recipes</a>
                                                 <a href="recipe-list.html" class="">Banana & Palm Sugar Parcels</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="slider-item">
                                        <div class="testimonial-item">

                                            <div class="product_inner_cms">
                                               
                                                    <img class="img-responsive" src="image/recipe/recipe.png">
                                                 <a href="recipe-list.html" class="recipe-text">View Recipes</a>
                                                 <a href="recipe-list.html" class="">Butter Chicken with Rice</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="slider-item">
                                        <div class="testimonial-item">

                                            <div class="product_inner_cms">
                                               
                                                    <img class="img-responsive" src="image/recipe/recipe2.png">
                                                <a href="recipe-list.html" class="recipe-text">View Recipes</a>
                                                 <a href="recipe-list.html" class="">Crispy Spring Rolls</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="slider-item">
                                        <div class="testimonial-item">

                                            <div class="product_inner_cms">
                                               
                                                    <img class="img-responsive" src="image/recipe/recipe.png">
                                                <a href="#" class="recipe-text">View Recipes</a>
                                                <a href="#" class="">Dutch Pea and Ham Soup</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row home_row">

        <div id="content" class="col-sm-12">
            <div class="hometab box">
                <div class="container">

                    <div class="box featured">
                        <div class="container">
                            <div class="box-head">
                                <div class="box-heading text-center"><a href="list-view.php">Miscellaneous</a></div>
                            </div>
                            <div class="box-content">
                                <div class="customNavigation">
                                    <a class="fa prev fa-arrow-left">&nbsp;</a>
                                    <a class="fa next fa-arrow-right">&nbsp;</a>
                                </div>

                                <div class="box-product product-carousel" id="featured-carousel">
                                    <div class="slider-item">
                                        <div class="product-block product-thumb transition">
                                            <div class="product-block-inner">
                                                <div class="image">
                                                    <a href="list-view.php">
                                                        <img src="image/list/14.png" title="Alpen Birthday Candle 24's" alt="Alpen Birthday Candle 24's" class="img-responsive list-img" />

                                                    </a>


                                                    <span class="saleicon sale">Sale</span>


                                                    <!--    <div class="percentsaving">5% off</div>-->

                                                    <div class="button-group">
                                                     
                                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                            <a class="quickbox" href="#">
                                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        </div>
                                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('40 ');"><i class="fa fa-exchange"></i></button>
                                                       
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                        <div class="product-details">
                                            <div class="caption">
                                                <h4 class="title-blue"><a href="list-view.php">Alpen Birthday Candle 24's </a></h4>
                                                <p>Pack Size: 12 x 24's</p>
                                                <p>Product Id: 410816</p>

                                                <div class="rating">
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> 1Review


                                                </div>
                                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="slider-item">
                                        <div class="product-block product-thumb transition">
                                            <div class="product-block-inner">
                                                <div class="image">
                                                    <a href="list-view.php">
                                                        <img src="image/list/3.jpg" title="Alpen Tealight Candle" alt="Alpen Tealight Candle" class="img-responsive list-img" />

                                                    </a>


                                                    <span class="saleicon sale">Sale</span>


                                                    <!-- <div class="percentsaving">8% off</div>-->

                                                    <div class="button-group">
                                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('42 ');"><i class="fa fa-heart"></i></button>
                                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                            <a class="quickbox" href="#">
                                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        </div>
                                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('42 ');"><i class="fa fa-exchange"></i></button>
                                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('42 ');"><span>Add to Cart</span> </button>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="product-details">
                                            <div class="caption">
                                                <h4 class="title-blue"><a href="list-view.php">Alpen Tealight Candle 4hr 50's </a></h4>
                                                <p>Pack Size: 12 x 50's</p>
                                                <p>Product Id: 415523</p>

                                                <div class="rating">
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> 2Reviews


                                                </div>
                                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="slider-item">
                                        <div class="product-block product-thumb transition">
                                            <div class="product-block-inner">
                                                <div class="image">
                                                    <a href="list-view.php">
                                                        <img src="image/list/2.jpg" title="Alpen Parasol Cocktail" alt="Alpen Parasol Cocktail" class="img-responsive list-img" />

                                                    </a>


                                                    <span class="saleicon sale">Sale</span>


                                                    <!--   <div class="percentsaving">11% off</div>-->

                                                    <div class="button-group">
                                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('30 ');"><i class="fa fa-heart"></i></button>
                                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                            <a class="quickbox" href="#">
                                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        </div>
                                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('30 ');"><i class="fa fa-exchange"></i></button>
                                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('30 ');"><span>Add to Cart</span> </button>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="product-details">
                                            <div class="caption">
                                                <h4 class="title-blue"><a href="list-view.php">Alpen Parasol Cocktail 100's </a></h4>
                                                <p>Pack Size: 50 x 100's</p>
                                                <p>Product Id: 850489</p>

                                                <div class="rating">
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> 1Review


                                                </div>
                                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="slider-item">
                                        <div class="product-block product-thumb transition">
                                            <div class="product-block-inner">
                                                <div class="image">
                                                    <a href="list-view.php">
                                                        <img src="image/list/1.jpg" title="Alpen Toothpick Double End" alt="Alpen Toothpick Double End" class="img-responsive list-img" />

                                                    </a>
                                                    <span class="saleicon sale">Sale</span>


                                                    <div class="button-group">
                                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('43 ');"><i class="fa fa-heart"></i></button>
                                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                            <a class="quickbox" href="#">
                                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        </div>
                                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('43 ');"><i class="fa fa-exchange"></i></button>
                                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('43 ');"><span>Add to Cart</span> </button>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="product-details">
                                            <div class="caption">
                                                <h4 class="title-blue"><a href="list-view.php">Alpen Toothpick Double End 1000's</a></h4>
                                                <p>Pack Size: 10 x 10000's</p>
                                                <p>Product Id: 420602</p>

                                                <div class="rating">
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> 1Review

                                                </div>
                                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="slider-item">
                                        <div class="product-block product-thumb transition">
                                            <div class="product-block-inner">
                                                <div class="image">
                                                    <a href="list-view.php">
                                                        <img src="image/list/15.png" title="Auscrown Butane Gas Cartridge 200g" alt="Auscrown Butane Gas Cartridge 200g" class="img-responsive list-img" />

                                                    </a>




                                                    <div class="button-group">
                                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('47 ');"><i class="fa fa-heart"></i></button>
                                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                            <a class="quickbox" href="#">
                                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        </div>
                                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('47 ');"><i class="fa fa-exchange"></i></button>
                                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('47 ');"><span>Add to Cart</span> </button>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="product-details">
                                            <div class="caption">
                                                <h4 class="title-blue"><a href="list-view.php">Auscrown Butane Gas Cartridge 200g</a></h4>
                                                <p>Pack Size: 28x238g</p>
                                                <p>Product ID: 571312</p>

                                                <div class="rating">
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> 1Review

                                                </div>
                                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="slider-item">
                                        <div class="product-block product-thumb transition">
                                            <div class="product-block-inner">
                                                <div class="image">
                                                    <a href="list-view.php">
                                                        <img src="image/list/16.png" title="Alpen Toothpicks Individual Wrap 1000’s" alt="Alpen Toothpicks Individual Wrap 1000’s" class="img-responsive list-img" />

                                                    </a>


                                                    <span class="saleicon sale">Sale</span>


                                                    <!-- <div class="percentsaving">8% off</div>-->

                                                    <div class="button-group">
                                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('42 ');"><i class="fa fa-heart"></i></button>
                                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                            <a class="quickbox" href="#">
                                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        </div>
                                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('42 ');"><i class="fa fa-exchange"></i></button>
                                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('42 ');"><span>Add to Cart</span> </button>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="product-details">
                                            <div class="caption">
                                                <h4 class="title-blue"><a href="list-view.php">Alpen Toothpicks Individual Wrap 1000’s</a></h4>
                                                <p>Pack Size: 50x1000's</p>
                                                <p>Product ID: 421111</p>

                                                <div class="rating">
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> 2Reviews


                                                </div>
                                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="slider-item">
                                        <div class="product-block product-thumb transition">
                                            <div class="product-block-inner">
                                                <div class="image">
                                                    <a href="list-view.php">
                                                        <img src="image/list/17.png" title="Alpen Sparklers 8’s" alt="Alpen Sparklers 8’s" class="img-responsive list-img" />

                                                    </a>


                                                    <span class="saleicon sale">Sale</span>


                                                    <!--   <div class="percentsaving">11% off</div>-->

                                                    <div class="button-group">
                                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('30 ');"><i class="fa fa-heart"></i></button>
                                                        <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                            <a class="quickbox" href="#">
                                                                <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        </div>
                                                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('30 ');"><i class="fa fa-exchange"></i></button>
                                                        <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('30 ');"><span>Add to Cart</span> </button>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="product-details">
                                            <div class="caption">
                                                <h4 class="title-blue"><a href="list-view.php">Alpen Sparklers 8’s</a></h4>
                                                <p>Pack Size: 288x8's</p>
                                                <p>Product ID: 890016</p>

                                                <div class="rating">
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> 1Review


                                                </div>
                                                <a class="wishlist"><i class="fa fa-heart"></i></a>
                                                <a class="wishlist"><i class="fa fa-balance-scale" aria-hidden="true"></i></a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="featured_default_width" style="display:none; visibility:hidden"></span>



                </div>
            </div>

            <script type="text/javascript">
                $('#categorytabs a').tabs();

            </script>




            <div class="box blogs">
                <div class="container">

                    <div class="box-head">
                        <div class="box-heading text-center">Events & Promotions</div>
                    </div>
                    <div class="box-content">
                        <div class="customNavigation">
                            <a class="fa prev czblog_prev"></a>
                            <a class="fa next czblog_next"></a>
                        </div>

                        <div class="box-product  owl-carousel blogcarousel " id="blog-carousel">

                            <div class="blog-item">
                                <div class="product-block">
                                    <div class="product-block-inner">

                                        <div class="blog-left">
                                            <div class="blog-image">
                                                <img src="image/events/e1.png" alt="Welcome To Our New Website" title="Welcome To Our New Website" class="img-thumbnail" />
                                                <div class="post-image-hover"> </div>
                                                <p class="post_hover"><a class="icon zoom" title="Click to view Full Image " href="image/cache/catalog/blog-6-895x556.jpg" data-lightbox="example-set"><i class="fa fa-search-plus"></i> </a>
                                                    <a class="icon readmore_link" title="Click to view Read More" href="#"><i class="fa fa-link"></i></a>
                                                </p>
                                            </div>

                                            <div class="date-comment">
                                                <div class="date-time"> 25 <br /> May </div>
                                            </div>

                                        </div>


                                        <div class="blog-right">
                                            <h4 class="blog_title"><a href="#">Curabitur at elit Vestibulum</a> </h4>
                                            <div class="blog-desc">Welcome to the all-new Hong Australia Corporation Website. We have been working hard to develop a brand new website which is user friendly to our customers both current and potential. We hope you enjoy this new look and feel of our brand new website. Hong Australia Corporation would like to aid their customers with</div>




                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">May 21st 2014</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">Admin</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">0</span>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-default btn-lg" onclick="return subscribe();">Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="blog-item">
                                <div class="product-block">
                                    <div class="product-block-inner">

                                        <div class="blog-left">
                                            <div class="blog-image">
                                                <img src="image/events/e2.png" alt="Welcome To Our New Website" title="Welcome To Our New Website" class="img-thumbnail" />
                                                <div class="post-image-hover"> </div>
                                                <p class="post_hover"><a class="icon zoom" title="Click to view Full Image " href="image/cache/catalog/blog-5-895x556.jpg" data-lightbox="example-set"><i class="fa fa-search-plus"></i> </a>
                                                    <a class="icon readmore_link" title="Click to view Read More " href="#"><i class="fa fa-link"></i></a>
                                                </p>
                                            </div>

                                            <div class="date-comment">
                                                <div class="date-time"> 25 <br /> May </div>
                                            </div>

                                        </div>


                                        <div class="blog-right">
                                            <h4 class="blog_title"><a href="#">Urna pretium elit mauris cursus</a> </h4>
                                            <div class="blog-desc">Welcome to the all-new Hong Australia Corporation Website. We have been working hard to develop a brand new website which is user friendly to our customers both current and potential. We hope you enjoy this new look and feel of our brand new website. Hong Australia Corporation would like to aid their customers with</div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">May 21st 2014</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">Admin</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">0</span>
                                            </div>
                                        </div>
                                        <a href="events.php" type="submit" class="btn btn-default btn-lg">Read More</a>
                                    </div>
                                </div>
                            </div>

                            <div class="blog-item">
                                <div class="product-block">
                                    <div class="product-block-inner">

                                        <div class="blog-left">
                                            <div class="blog-image">
                                                <img src="image/events/e3.png" alt="Welcome To Our New Website" title="Welcome To Our New Website" class="img-thumbnail" />
                                                <div class="post-image-hover"> </div>
                                                <p class="post_hover"><a class="icon zoom" title="Click to view Full Image " href="image/cache/catalog/blog-4-895x556.jpg" data-lightbox="example-set"><i class="fa fa-search-plus"></i> </a>
                                                    <a class="icon readmore_link" title="Click to view Read More " href="#"><i class="fa fa-link"></i></a>
                                                </p>
                                            </div>

                                            <div class="date-comment">
                                                <div class="date-time"> 25 <br /> May </div>
                                            </div>

                                        </div>


                                        <div class="blog-right">
                                            <h4 class="blog_title"><a href="#">Nullam ullamcorper ornare molestie</a> </h4>
                                            <div class="blog-desc"> Welcome to the all-new Hong Australia Corporation Website. We have been working hard to develop a brand new website which is user friendly to our customers both current and potential. We hope you enjoy this new look and feel of our brand new website. Hong Australia Corporation would like to aid their customers with </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">May 21st 2014</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">Admin</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">0</span>
                                            </div>
                                        </div>
                                         <a href="events2.php" type="submit" class="btn btn-default btn-lg">Read More</a>
                                    </div>
                                </div>
                            </div>

                            <div class="blog-item">
                                <div class="product-block">
                                    <div class="product-block-inner">

                                        <div class="blog-left">
                                            <div class="blog-image">
                                                <img src="image/events/e1.png" alt="Welcome To Our New Website" title="Welcome To Our New Website" class="img-thumbnail" />
                                                <div class="post-image-hover"> </div>
                                                <p class="post_hover"><a class="icon zoom" title="Click to view Full Image " href="image/cache/catalog/blog-3-895x556.jpg" data-lightbox="example-set"><i class="fa fa-search-plus"></i> </a>
                                                    <a class="icon readmore_link" title="Click to view Read More " href="#"><i class="fa fa-link"></i></a>
                                                </p>
                                            </div>

                                            <div class="date-comment">
                                                <div class="date-time"> 25 <br /> May </div>
                                            </div>

                                        </div>


                                        <div class="blog-right">
                                            <h4 class="blog_title"><a href="#">Turpis at eleifend Aenean porta</a> </h4>
                                            <div class="blog-desc"> Welcome to the all-new Hong Australia Corporation Website. We have been working hard to develop a brand new website which is user friendly to our customers both current and potential. We hope you enjoy this new look and feel of our brand new website. Hong Australia Corporation would like to aid their customers with</div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">May 21st 2014</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">Admin</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">0</span>
                                            </div>
                                        </div>
                                        <a href="event3.php" type="submit" class="btn btn-default btn-lg">Read More</a>
                                    </div>
                                </div>
                            </div>

                            <div class="blog-item">
                                <div class="product-block">
                                    <div class="product-block-inner">

                                        <div class="blog-left">
                                            <div class="blog-image">
                                                <img src="image/events/e2.png" alt="Latest Blog" title="Latest Blog" class="img-thumbnail" />
                                                <div class="post-image-hover"> </div>
                                                <p class="post_hover"><a class="icon zoom" title="Click to view Full Image " href="image/cache/catalog/blog-2-895x556.jpg" data-lightbox="example-set"><i class="fa fa-search-plus"></i> </a>
                                                    <a class="icon readmore_link" title="Click to view Read More " href="#"><i class="fa fa-link"></i></a>
                                                </p>
                                            </div>

                                            <div class="date-comment">
                                                <div class="date-time"> 25 <br /> May </div>
                                            </div>

                                        </div>


                                        <div class="blog-right">
                                            <h4 class="blog_title"><a href="#">Morbi condimentum molestie Nam</a> </h4>
                                            <div class="blog-desc"> Welcome to the all-new Hong Australia Corporation Website. We have been working hard to develop a brand new website which is user friendly to our customers both current and potential. We hope you enjoy this new look and feel of our brand new website. Hong Australia Corporation would like to aid their customers with</div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">May 21st 2014</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">Admin</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">0</span>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-default btn-lg" onclick="return subscribe();">Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="blog-item">
                                <div class="product-block">
                                    <div class="product-block-inner">

                                        <div class="blog-left">
                                            <div class="blog-image">
                                                <img src="image/events/e3.png" alt="Welcome To Our New Website" title="Welcome To Our New Website" class="img-thumbnail" />
                                                <div class="post-image-hover"> </div>
                                                <p class="post_hover"><a class="icon zoom" title="Click to view Full Image " href="image/cache/catalog/blog-1-895x556.jpg" data-lightbox="example-set"><i class="fa fa-search-plus"></i> </a>
                                                    <a class="icon readmore_link" title="Click to view Read More " href="#"><i class="fa fa-link"></i></a>
                                                </p>
                                            </div>

                                            <div class="date-comment">
                                                <div class="date-time"> 25 <br /> May </div>
                                            </div>

                                        </div>


                                        <div class="blog-right">
                                            <h4 class="blog_title"><a href="#">Curabitur at elit Vestibulum</a> </h4>
                                            <div class="blog-desc">Welcome to the all-new Hong Australia Corporation Website. We have been working hard to develop a brand new website which is user friendly to our customers both current and potential. We hope you enjoy this new look and feel of our brand new website. Hong Australia Corporation would like to aid their customers with</div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">May 21st 2014</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">Admin</span>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <span class="blogg">0</span>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-default btn-lg" onclick="return subscribe();">Read More</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="buttons text-center">
                            <button type="button" onclick="location='#'" class="btn btn-primary">See all Blogs</button>
                        </div>
                    </div>
                </div>
            </div>
            <span class="blog_default_width" style="display:none; visibility:hidden"></span>

            <script type="text/javascript">
                <!--

                $(document).ready(function() {
                    $('.blogcarousel').owlCarousel({
                        items: 3,
                        singleItem: false,
                        navigation: false,
                        pagination: false,
                        itemsDesktop: [1199, 2],
                        itemsDesktopSmall: [991, 2],
                        itemsTablet: [575, 1]
                    });
                    // Custom Navigation Events
                    $(".czblog_next").click(function() {
                        $('.blogcarousel').trigger('owl.next');
                    })
                    $(".czblog_prev").click(function() {
                        $('.blogcarousel').trigger('owl.prev');
                    });
                });
                -->

            </script>
            <div class="box blogs text-center">
                <div class="container">
                    <div class="box-head">
                        <div class="box-heading text-center">Our Products</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="p-image">
                                <img class="img-responsive" src="image/products/p1.png">
                            </div>
                            <div class="product-details">
                                <div class="caption">
                                    <h4 class="title"><a href="#">Utensils</a></h4>
                                    <p>Cutlery,Work,Filters</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="p-image">
                               <a href="#"> <img class="img-responsive" src="image/products/p2.png"></a>
                            </div>
                            <div class="product-details">
                                <div class="caption">
                                    <h4 class="title"><a href="#">New Products</a></h4>
                                    <p>New & Exciting Products</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="p-image">
                                <img class="img-responsive" src="image/products/p3.png">
                            </div>
                            <div class="product-details">
                                <div class="caption">
                                    <h4 class="title"><a href="#">Dried Food</a></h4>
                                    <p>Biscuits,Beverages,Spices</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="p-image">
                                <img class="img-responsive" src="image/products/p4.png">
                            </div>
                            <div class="product-details">
                                <div class="caption">
                                    <h4 class="title"><a href="#">Cleaning Products</a></h4>
                                    <p>Household Cleaning</p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="p-image">
                                <img class="img-responsive" src="image/products/p5.png">
                            </div>
                            <div class="product-details">
                                <div class="caption">
                                    <h4 class="title"><a href="#">Chilled Food</a></h4>
                                    <p>Butter,Cheese,Cream,Milk</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="p-image">
                                <img class="img-responsive" src="image/products/p6.png">
                            </div>
                            <div class="product-details">
                                <div class="caption">
                                    <h4 class="title"><a href="#">Frozen Food</a></h4>
                                    <p>Finger foods,Ice cream</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="p-image">
                                <img class="img-responsive" src="image/products/p7.png">
                            </div>
                            <div class="product-details">
                                <div class="caption">
                                    <h4 class="title"><a href="#">Paper & Packaging</a></h4>
                                    <p>Cups,Plates,Cutlery,Food containers...</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="p-image">
                                <img class="img-responsive" src="image/products/p8.png">
                            </div>
                            <div class="product-details">
                                <div class="caption">
                                    <h4 class="title"><a href="#">Miscellaneous</a></h4>
                                    <p>Extra items,Other food</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="our-products text-center">

            </div>
            <div id="carousel-0" class="banners-slider-carousel box">
                <div class="container">

                    <div class="carousel-block">
                        <div class="customNavigation">
                            <a class="prev fa fa-arrow-left">&nbsp;</a>
                            <a class="next fa fa-arrow-right">&nbsp;</a>
                        </div>
                        <div class="owl-carousel brand-carousel" id="module-0-carousel">
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c1.png" alt="NFL" />
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c2.png" alt="RedBull" />
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c3.png" alt="Sony" />
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c4.png" alt="Coca Cola" />
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c1.png" alt="Burger King" />
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c2.png" alt="Canon" />
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c3.png" alt="Harley Davidson" />
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c4.png" alt="Dell" />
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c1.png" alt="Disney" />
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c2.png" alt="Starbucks" />
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item">
                                <div class="product-block">
                                    <div class="product-block-inner">
                                        <img class="img-responsive" src="image/clients/c3.png" alt="Nintendo" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="module_default_width" style="display:none; visibility:hidden"></span>
        </div>

    </div>


    <footer>
       <?php include_once'footer.php';?>
    </footer>


</body>

</html>

<script type="text/javascript">
    $(document).ready(function() {
        $('#testimonial-carousel').owlCarousel({
            items: 3,
            singleItem: false,
            navigation: false,
            pagination: true,
            autoPlay: true,
            itemsDesktop: [1199, 2],
            itemsDesktopSmall: [991, 2],
            itemsTablet: [575, 1]
        });

        $('.brand-carousel').owlCarousel({
            items: 6,
            itemsDesktop: [1199, 5],
            itemsDesktopSmall: [991, 4],
            itemsTablet: [650, 3],
            itemsMobile: [400, 1],
            singleItem: false,
            navigation: false,
            pagination: false,
            autoPlay: true
        });
    });

</script>
