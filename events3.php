<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en">
<!--<![endif]-->
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include_once'meta.php';?>
    <meta name="description" content="Fruit Store Opencart Responsive Theme is designed for Organic, Grocery, Food, Vegetables, Fruits, Minimal   and multi purpose stores. This Theme is looking good with colors combination. It is very nice with its clean and professional look." />
    <meta name="keywords" content="Organic, Grocery, Food, Vegetables, Fruits, Minimal" />

    <script src="assets/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="assets/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="assets/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="assets/view/theme/Frruit/stylesheet/stylesheet.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="assets/view/javascript/jquery/magnific/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/custom.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/lightbox.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/animate.css" />




    <link href="assets/view/javascript/jquery/swiper/css/swiper.min.css" type="text/css" rel="stylesheet" media="screen" />
    <link href="assets/view/javascript/jquery/swiper/css/opencart.css" type="text/css" rel="stylesheet" media="screen" />
    <script src="assets/view/javascript/jquery/swiper/js/swiper.jquery.js" type="text/javascript"></script>

    <script type="text/javascript" src="assets/view/javascript/codezeel/custom.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jstree.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/carousel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/codezeel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.custom.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.formalize.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/lightbox/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/tabs.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.elevatezoom.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/doubletaptogo.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/parallax.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>

    <script src="assets/view/javascript/common.js" type="text/javascript"></script>
</head>


<body class="common-home layout-1">
  <nav id="top">
        <?php include_once'top_header.php';?>
    </nav>


    <header>
        <?php include_once'header.php';?>
       
    </header>


    <div class="wrap-breadcrumb parallax-breadcrumb">
        <div class="container"></div>
    </div>

    <!-- ======= Quick view JS ========= -->
    <script>
        function quickbox() {
            if ($(window).width() > 767) {
                $('.quickview-button').magnificPopup({
                    type: 'iframe',
                    delegate: 'a',
                    preloader: true,
                    tLoading: 'Loading image #%curr%...',
                });
            }
        }
        jQuery(document).ready(function() {
            quickbox();
        });
        jQuery(window).resize(function() {
            quickbox();
        });

    </script>
    
    <div class="container">
        <div class="row category_thumb">
            <div class="col-sm-2 category_img"><img class="img-responsive" src="image/banner/banner2.png" alt="Citrus Frruit" title="Citrus Frruit" class="img-responsive"></div>

        </div>

    </div>
    <div class="wrap-breadcrumb parallax-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                <li>Events & Promo's</li>


            </ul>
        </div>
    </div>
   


    <div id="czservicecmsblock">
        <h1 class="h1 products-section-title text-uppercase text-center"><span>Events & Promotions</span></h1>
        <div class="service_container">
            <div class="service-area">
                <div class="col-md-7 service-left-part">
                    <div class="service-bottom-inner">
                        <div class="service-third service1">
                            <!-- <div class="service-icon icon1"></div>-->
                            <div class="service-content">
                                <div class="service-description">
                                    <p>Welcome to the all-new Hong Australia Corporation Website. We have been working hard to develop a brand new website which is user friendly to our customers both current and potential. We hope you enjoy this new look and feel of our brand new website.</p>
                                </div>
                            </div>
                        </div>
                        <div class="service-third service2">
                            <!-- <div class="service-icon icon2"></div>-->
                            <div class="service-content">
                                <div class="service-description">Hong Australia Corporation would like to aid their customers with the ability to search for particular products more easily, let customers browse through our diverse range of products and let customers know of any new promotions or events that Hong Australia Corporation are holding. Furthermore to this, why not try out one of our recipes, which is sure to be a crowd favourite at any lunch or dinner party or even for a meal with your family and friends.</div>
                            </div>
                        </div>
                        <div class="service-third service3">
                            <!-- <div class="service-icon icon3"></div>-->
                            <div class="service-content">
                                <div class="service-description">The new website will allow us to communicate further with our customers. Our customers both current and potential are very important to us. At Hong Australia Corporation, we want to provide a high quality service face to face, over the phone and online.If you have any further enquiries, please do not hesitate to contact our staff on the phone or send us a query online. Our staff are always ready to answer all your questions and queries that you may have.</div>
                            </div>
                        </div>
                        <div class="service-third service3">
                            <!-- <div class="service-icon icon3"></div>-->
                            <div class="service-content">
                                <div class="service-description">Hong Australia Corporation Management

                                </div>
                            </div>
                        </div>
                    </div>
                   

                </div>
                <div class="col-md-5 service-right-part">

                    <!--  <div class="service-banner2"><a href="#" class="service-anchor"><img src="image/catalog/service-banner2.png" alt="service-banner2"></a></div>-->
                    <div class="service-banner1">
                        <a href="#" class="service-anchor"><img class="img-responsive about-b" src="image/events/e3.png" alt="service-banner1"></a>
                    </div>

                </div>
            </div>
        </div>
    </div>


     <footer>
       <?php include_once'footer.php';?>
    </footer>


</body>

</html>

<script type="text/javascript">
    $(document).ready(function() {
        $('#testimonial-carousel').owlCarousel({
            items: 3,
            singleItem: false,
            navigation: false,
            pagination: true,
            autoPlay: true,
            itemsDesktop: [1199, 2],
            itemsDesktopSmall: [991, 2],
            itemsTablet: [575, 1]
        });

        $('.brand-carousel').owlCarousel({
            items: 6,
            itemsDesktop: [1199, 5],
            itemsDesktopSmall: [991, 4],
            itemsTablet: [650, 3],
            itemsMobile: [400, 1],
            singleItem: false,
            navigation: false,
            pagination: false,
            autoPlay: true
        });
    });

</script>
