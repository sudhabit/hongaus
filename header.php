<div class="header-container">
            <div class="container">
                <div class="row">
                    <div class="header-main">
                        <div class="header-logo">
                            <div id="logo">
                                <a href="index.php"><img src="image/clients/c2.png" title="" alt="" class="img-responsive" /></a>
                            </div>
                        </div>

                        <div class="header-top-inner">
                            <div class="container">
                                <nav class="nav-container" role="navigation">
                                    <div class="nav-inner container">
                                        <!-- ======= Menu Code START ========= -->
                                        <!-- Opencart 3 level Category Menu-->
                                        <div id="menu" class="main-menu">

                                            <ul class="nav navbar-nav">
                                                <li class="top_level dropdown home"><a href="dried-food.php">Dried Food</a>
                                                </li>

                                                <li class="top_level dropdown"><a href="chilled-food.php">Chilled Food</a>

                                                  
                                                </li>
                                                <li class="top_level"><a href="frozen-food.php">Frozen Food</a></li>
                                                <li class="top_level"><a href="paper-pack.php">Paper & Packing</a></li>
                                                <li class="top_level dropdown"><a href="cleaning-pro.php">Cleaning Products</a>

                                                 
                                                </li>
                                                <li class="top_level"><a href="list-view.php">Miscellaneous</a></li>
                                                <li class="top_level">
                                                    <a href="#"> <span class="input-group-btn">
                                                            <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
                                                        </span></a>
                                                </li>


                                            </ul>
                                        </div>
                                    </div>



                                    <!--  =============================================== Mobile menu start  =============================================  -->
                                    <div id="res-menu" class="main-menu nav-container1 ">
                                        <div class="nav-responsive"><span>Menu</span>
                                            <div class="expandable"></div>
                                        </div>
                                        <ul class="main-navigation">
                                            <li class="top_level dropdown"><a href="dried-food.php">Dried Food</a>
                                               
                                            </li>
                                            <li class="top_level dropdown"><a href="chilled-food.php">Chilled Food</a>
                                            </li>
                                            <li class="top_level dropdown"><a href="frozen-food.php">Frozen Food</a>
                                            </li>
                                            <li class="top_level dropdown"><a href="paper-pack.php">Paper & Packing</a>
                                             
                                            </li>
                                            <li class="top_level dropdown"><a href="cleaning-pro.php">Cleaning Products</a>
                                            </li>
                                            <li class="top_level dropdown"><a href="list-view.php">Miscellaneous</a>
                                            </li>

                                            <li class="top_level">
                                                <a href="#"> <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
                                                    </span></a>
                                            </li>
                                        </ul>
                                    </div>


                                </nav>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }
	function triggerHtmlEvent(element, eventName) {
	  var event;
	  if (document.createEvent) {
		event = document.createEvent('HTMLEvents');
		event.initEvent(eventName, true, true);
		element.dispatchEvent(event);
	  } else {
		event = document.createEventObject();
		event.eventType = eventName;
		element.fireEvent('on' + event.eventType, event);
	  }
	}
    $(document).ready(function(){
		$('.lang-select').on("click", function(){
		  var theLang = $(this).attr('data-lang');
		  $('.goog-te-combo').val(theLang);
		  window.location = $(this).attr('href');
		  location.reload();
		});
	});
  </script>