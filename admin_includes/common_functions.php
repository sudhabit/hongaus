<?php
   
    function userLogin($user_email,$user_password) {
        global $conn;
        $sql="SELECT * FROM users WHERE (user_email = '$user_email' OR user_mobile = '$user_email') AND user_password = '$user_password' ";
        $result = $conn->query($sql);        
        return $result;
    }

    function getRowsWhereCount($table,$clause,$value)  {
        global $conn;
        $sql="select * from `$table` WHERE `$clause` = '$value' ";
        $result = $conn->query($sql);
        $noRows = $result->num_rows;
        return $noRows;
    }
    function checkUserAvail($table,$clause,$value){
        global $conn;
        $sql = "SELECT * FROM `$table` WHERE `$clause` LIKE '$value' ";
        $result = $conn->query($sql);
        return $result->num_rows;
    }

    function sendEmail($to,$subject,$message,$from,$name) {
        global $conn;   
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";  
        $headers .= 'From: '.$name.'<'.$from.'>'. "\r\n";
        if(mail($to, $subject, $message, $headers)) {
            return 0;
        } else {
            return 1;
        }

    }

    function getAllData($table)  {
        global $conn;
        $sql="select * from `$table` ";
        $result = $conn->query($sql);        
        return $result;
    }

    function getAllDataWithActiveRecent($table)  {
        global $conn;
        $sql="select * from `$table` ORDER BY is_active, id DESC ";
        $result = $conn->query($sql); 
        return $result;
    }

    function getIndividualDetails($table,$clause,$id)  {
        global $conn;
        $sql="select * from `$table` where `$clause` = '$id' ";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();        
        return $row;
    }

    function getAllDataWhere($table,$clause,$value)  {
        global $conn;
        $sql="select * from `$table` WHERE `$clause` = '$value' ";
        $result = $conn->query($sql);        
        return $result;
    }

    function getAllDataWhereWithActive($table,$clause,$value)  {
        global $conn;
        $sql="select * from `$table` WHERE `$clause` = '$value' AND lkp_status_id = '0' order by id DESC";
        $result = $conn->query($sql);        
        return $result;
    }

    function getAllDataWithStatus($table,$status)  {
        global $conn;
        $sql="select * from `$table` WHERE `is_active` = '$status' ";
        $result = $conn->query($sql); 
        return $result;
    }

     function getWishListCount($table,$user_id,$product_id)  {
        global $conn;
        $sql="select * from `$table` WHERE `user_id` = '$user_id' AND product_id='$product_id' ";
        $result = $conn->query($sql);
        $noRows = $result->num_rows;
        return $noRows;
    }
    function getRowsCount($table)  {
        global $conn;
        $sql="select * from `$table` WHERE lkp_status_id='0'";
        $result = $conn->query($sql);
        $noRows = $result->num_rows;
        return $noRows;
    }

    /* encrypt and decrypt password for login*/
     function encryptPassword($pwd) {
        $key = "123";
        $admin_pwd = bin2hex(openssl_encrypt($pwd,'AES-128-CBC', $key));
        return $admin_pwd;
    }

    function decryptPassword($admin_password) {
        $key = "123";
        $admin_pwd = openssl_decrypt(hex2bin($admin_password),'AES-128-CBC',$key);
        return $admin_pwd;
    }

    /* encrypt and decrypt password */
     function encryptPassword1($pwd) {
        $key = "123";
        $admin_pwd = openssl_encrypt($pwd,'AES-128-CBC', $key);
        return $admin_pwd;
    }

    function decryptPassword1($admin_password) {
        $key = "123";
        $admin_pwd = openssl_decrypt($admin_password,'AES-128-CBC',$key);
        return $admin_pwd;
    }

    function getImageUnlink($val,$table,$clause,$id,$target_dir) {
        global $conn;
        $getBanner = "SELECT $val FROM $table WHERE $clause='$id' ";
        $getRes = $conn->query($getBanner);
        $row = $getRes->fetch_assoc();
        $img = $row[$val];
        $path = $target_dir.$img.'';
        chown($path, 666);
        if (unlink($path)) {
            return 1;
        } else {
            return 0;
        }
    }


    function saveAdminLogs($service_id,$user_id,$message) {
        //Save data into logs table
        global $conn;   
        $remote_addr = $_SERVER['REMOTE_ADDR'];
        $log_start_date = date("Y-m-d h:i:s");

       $sqlIns = "INSERT INTO admin_log_history (remote_addr,message,log_start_date,service_id,user_id) VALUES ('$remote_addr','$message','$log_start_date','$service_id','$user_id')";
        if ($conn->query($sqlIns) === TRUE) {
            return 1;
        } else {
            return 0;
        } 
    }

    function changeDateFormat($date) {
        //Save data into logs table
        $yrdata= strtotime($date);
        return date('M d , Y', $yrdata);
    }

    function changeDateFormat1($date) {
        //Save data into logs table
        $yrdata= strtotime($date);
        return date('M d , Y H:i:s', $yrdata);
    }

    function dateFormat($date) {
        //Save data into logs table
        $yrdata= strtotime($date);
        return date('d/m/Y H:i:s', $yrdata);
    }

    function dateFormat1($date) {
        //Save data into logs table
        $yrdata= strtotime($date);
        return date('d/m/Y', $yrdata);
    }

    function updateAdminLogs($service_id,$user_id) {
        //Update User Log end time
        global $conn;
        $log_end_date = date("Y-m-d h:i:s");

        $getAdminLRec = "SELECT * FROM admin_log_history WHERE service_id='$service_id' AND user_id ='$user_id' ORDER BY log_id DESC LIMIT 1";
        $geLaRec = $conn->query($getAdminLRec);
        $getRecRecord = $geLaRec->fetch_assoc();
        $getLogId= $getRecRecord['log_id'];
        $updateLog= "UPDATE admin_log_history SET log_end_date='$log_end_date' WHERE user_id='$user_id' AND service_id='$service_id' AND log_id='$getLogId' ";
        $conn->query($updateLog);
        return $getLogId;
    }
    
    function queryExecute($sql)
    {
        global $conn;
        //example  mysql_query ;
        $res=$conn->query($sql); ;
        return $res;
    }
    function getColumnData($sql){
       global $conn;        
       $result = $conn->query($sql);
       $row = $result->fetch_assoc();        
       return $row;
   }
    function getCategoryName($id){
        $data = "select category_name from `categories` where id='$id'";
        $dataVal = getColumnData($data);
        return $dataVal['category_name'];
    }

?>
