<div class="container">
    <div id="top-links" class="nav pull-left">
        <div class="lang-curr">
            Default Welcome msg!
        </div>

    </div>

    <div class="headertopright">


        <div class="pull-left">

            <div class="lang-curr">
                <a href="about.php">About</a>
                <a href="recipe-list.php">Recipes</a>
                <a href="events.php">Events & Promo</a>
                <a href="#">Contact Us</a>
                
            </div>
            <ul class="list-unstyled list-inline ct-topbar__list">
                    <li class="ct-language">Language <i class="fa fa-arrow-down"></i>
                        <ul class="list-unstyled ct-language__dropdown">
                            <li><a href="#googtrans(en|en)" class="lang-en lang-select" data-lang="en">ENGLISH</a></li>
                            <li><a href="#googtrans(en|zh-CN)" class="lang-es lang-select" data-lang="zh-CN">CHINESE</a></li>

                        </ul>
                    </li>
                </ul>
        </div>

    </div>
</div>
