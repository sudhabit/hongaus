<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en">
<!--<![endif]-->
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include_once'meta.php';?>

    <script src="assets/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="assets/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="assets/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="assets/view/theme/Frruit/stylesheet/stylesheet.css" rel="stylesheet" />

    <!-- Codezeel www.codezeel.com - Start -->
    <link rel="stylesheet" type="text/css" href="assets/view/javascript/jquery/magnific/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/custom.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/lightbox.css" />
    <link rel="stylesheet" type="text/css" href="assets/view/theme/Frruit/stylesheet/codezeel/animate.css" />

    <link href="assets/view/javascript/jquery/magnific/magnific-popup.css" type="text/css" rel="stylesheet" media="screen" />
    <link href="assets/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
    <script src="assets/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
    <script src="assets/view/javascript/jquery/datetimepicker/moment/moment.min.js" type="text/javascript"></script>
    <script src="assets/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="assets/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="assets/view/javascript/codezeel/custom.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jstree.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/carousel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/codezeel.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.custom.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.formalize.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/lightbox/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/tabs.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/jquery.elevatezoom.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/doubletaptogo.js"></script>
    <script type="text/javascript" src="assets/view/javascript/codezeel/owl.carousel.min.js"></script>


    <script src="assets/view/javascript/common.js" type="text/javascript"></script>
</head>


<body class="product-product-41 layout-1">
    <nav id="top">
        <?php include_once'top_header.php';?>
    </nav>


    <header>
        <?php include_once'header.php';?>

    </header>
    <div class="container">
        <div class="row category_thumb">
            <div class="col-sm-2 category_img"><img class="img-responsive" src="image/banner/banner2.png" alt="Citrus Frruit" title="Citrus Frruit" class="img-responsive"></div>

        </div>

    </div>
    <div class="wrap-breadcrumb parallax-breadcrumb">
        <div class="container"></div>
    </div>

    <!-- ======= Quick view JS ========= -->
    <script>
        function quickbox() {
            if ($(window).width() > 767) {
                $('.quickview-button').magnificPopup({
                    type: 'iframe',
                    delegate: 'a',
                    preloader: true,
                    tLoading: 'Loading image #%curr%...',
                });
            }
        }
        jQuery(document).ready(function() {
            quickbox();
        });
        jQuery(window).resize(function() {
            quickbox();
        });

    </script>
    <div id="product-product" class="container">
        <ul class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-home"></i></a></li>
            <li>Paper & Packing</li>


        </ul>
        <div class="row">
            <div id="content" class="col-sm-12 productpage">
                <div class="row">
                    <div class="col-sm-5 product-left">
                        <div class="product-info">
                            <div class="left product-image thumbnails">

                                <!-- Codezeel Cloud-Zoom Image Effect Start -->
                                <div class="image">
                                    <a class="thumbnail" title="Nostrud Exercitation"><img id="tmzoom" src="image/banner/prod.jpg" data-zoom-image="#" title="Butter Chicken with Rice " alt="Butter Chicken with Rice " /></a>
                                </div>



                            </div>
                        </div>
                    </div>

                    <div class="col-sm-7 product-right">

                        <h3 class="product-title">Paper & Packing</h3>


                        <ul class="list-unstyled attr">
                            
                            <li><a href="beverages.php">Beverages</a></li>
                            <li><a href="list-view.php">Beans, Grains, Nuts &amp; Seeds</a></li>
                            <li><a href="list-view.php">Biscuits &amp; Snacks</a></li>
                            <li><a href="list-view.php">Booster</a></li>
                            <li><a href="list-view.php">Bread</a></li>
                            <li><a href="list-view.php">Cake</a></li>
                            <li><a href="list-view.php">Cake Ingredients</a></li>
                            <li><a href="list-view.php">Cake Premixes</a></li>
                            <li><a href="list-view.php">Cereal</a></li>
                            <li><a href="list-view.php">Chilli</a></li>
                            <li><a href="list-view.php">Chips</a></li>
                            <li><a href="list-view.php">Coconut &amp; Coconut Cream</a></li>
                            <li><a href="list-view.php">Condiments &amp; Dressings</a></li>
                            <li><a href="list-view.php">Confectionary</a></li>
                            <li><a href="list-view.php">Dessert</a></li>
                           
                        </ul>
                       
                    </div>

                </div>

                <div class="box related text-center">
                    <div class="box-head">
                        <div class="box-heading">Related Products</div>
                    </div>

                    <div class="box-content">
                        <div id="products-related" class="related-products">



                            <div class="customNavigation p-desc">
                                <a class="fa prev fa-arrow-left">&nbsp;</a>
                                <a class="fa next fa-arrow-right">&nbsp;</a>
                            </div>

                            <div class="box-product product-carousel" id="related-carousel">
                                <div class="slider-item">
                                    <div class="product-block product-thumb transition">
                                        <div class="product-block-inner">
                                            <div class="image">
                                                <a href="#">
                                                    <img src="image/recipe/d1.png" title="Knorr Butter Chicken Sauce 2.2kg" alt="Knorr Butter Chicken Sauce 2.2kg" class="img-responsive list-img" />

                                                </a>


                                                <span class="saleicon sale">Sale</span>


                                                <div class="percentsaving">11% off</div>

                                                <div class="button-group">
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('30 ');">
                                                        <i class="fa fa-heart"></i></button>
                                                    <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                        <a class="quickbox" href="indexeca1.html?route=product/quick_view&amp;product_id=30">
                                                            <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    </div>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('30 ');">
                                                        <i class="fa fa-exchange"></i></button>
                                                    <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('30 ');"><span>Add to Cart</span></button>
                                                </div>

                                            </div>


                                            <span class="related_default_width" style="display:none; visibility:hidden"></span>
                                        </div>

                                    </div>
                                    <div class="product-details">
                                        <div class="caption">
                                            <h4><a href="#">Knorr Butter Chicken Sauce 2.2kg</a></h4>


                                            <p class="price">
                                                Size: 2x2.2kg
                                            </p>
                                            <p class="price">
                                                Product ID: 640532

                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="product-block product-thumb transition">
                                        <div class="product-block-inner">
                                            <div class="image">
                                                <a href="#">
                                                    <img src="image/recipe/d2.png" title="Devondale Original Butter 500g" alt="Devondale Original Butter 500g" class="img-responsive list-img" />

                                                </a>


                                                <span class="saleicon sale">Sale</span>


                                                <div class="percentsaving">11% off</div>

                                                <div class="button-group">
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('30 ');">
                                                        <i class="fa fa-heart"></i></button>
                                                    <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                        <a class="quickbox" href="indexeca1.html?route=product/quick_view&amp;product_id=30">
                                                            <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    </div>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('30 ');">
                                                        <i class="fa fa-exchange"></i></button>
                                                    <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('30 ');"><span>Add to Cart</span></button>
                                                </div>

                                            </div>


                                            <span class="related_default_width" style="display:none; visibility:hidden"></span>
                                        </div>

                                    </div>
                                    <div class="product-details">
                                        <div class="caption">
                                            <h4><a href="#">Devondale Original Butter 500g</a></h4>


                                            <p class="price">
                                                Size: 12x500g
                                            </p>
                                            <p class="price">
                                                Product ID: 270900

                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="product-block product-thumb transition">
                                        <div class="product-block-inner">
                                            <div class="image">
                                                <a href="#">
                                                    <img src="image/recipe/d3.png" title="Royles Tomato Paste 2.15kg" alt="Royles Tomato Paste 2.15kg" class="img-responsive list-img" />

                                                </a>


                                                <span class="saleicon sale">Sale</span>


                                                <div class="percentsaving">11% off</div>

                                                <div class="button-group">
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('30 ');">
                                                        <i class="fa fa-heart"></i></button>
                                                    <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                        <a class="quickbox" href="indexeca1.html?route=product/quick_view&amp;product_id=30">
                                                            <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    </div>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('30 ');">
                                                        <i class="fa fa-exchange"></i></button>
                                                    <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('30 ');"><span>Add to Cart</span></button>
                                                </div>

                                            </div>


                                            <span class="related_default_width" style="display:none; visibility:hidden"></span>
                                        </div>

                                    </div>
                                    <div class="product-details">
                                        <div class="caption">
                                            <h4><a href="#">Royles Tomato Paste 2.15kg</a></h4>


                                            <p class="price">
                                                Size: 6x2.15kg
                                            </p>
                                            <p class="price">
                                                Product ID: 660077



                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="product-block product-thumb transition">
                                        <div class="product-block-inner">
                                            <div class="image">
                                                <a href="#">
                                                    <img src="image/recipe/d4.png" title="Anchor Cooking Cream 1L" alt="Anchor Cooking Cream 1L" class="img-responsive list-img" />

                                                </a>


                                                <span class="saleicon sale">Sale</span>


                                                <div class="percentsaving">11% off</div>

                                                <div class="button-group">
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('30 ');">
                                                        <i class="fa fa-heart"></i></button>
                                                    <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                        <a class="quickbox" href="indexeca1.html?route=product/quick_view&amp;product_id=30">
                                                            <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    </div>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('30 ');">
                                                        <i class="fa fa-exchange"></i></button>
                                                    <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('30 ');"><span>Add to Cart</span></button>
                                                </div>

                                            </div>


                                            <span class="related_default_width" style="display:none; visibility:hidden"></span>
                                        </div>

                                    </div>
                                    <div class="product-details">
                                        <div class="caption">
                                            <h4><a href="#">Anchor Cooking Cream 1L</a></h4>


                                            <p class="price">
                                                Size: 12x1L
                                            </p>
                                            <p class="price">
                                                Product ID: 270122

                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="slider-item">
                                    <div class="product-block product-thumb transition">
                                        <div class="product-block-inner">
                                            <div class="image">
                                                <a href="#">
                                                    <img src="image/recipe/d5.png" title="Wayang Palm Sugar 500g" alt="Wayang Palm Sugar 500g" class="img-responsive list-img" />

                                                </a>


                                                <span class="saleicon sale">Sale</span>


                                                <div class="percentsaving">11% off</div>

                                                <div class="button-group">
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('30 ');">
                                                        <i class="fa fa-heart"></i></button>
                                                    <div class="quickview-button" data-toggle="tooltip" title="Quick view">
                                                        <a class="quickbox" href="indexeca1.html?route=product/quick_view&amp;product_id=30">
                                                            <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    </div>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare " onclick="compare.add('30 ');">
                                                        <i class="fa fa-exchange"></i></button>
                                                    <button type="button" class="addtocart" data-toggle="tooltip" title="Add to Cart " onclick="cart.add('30 ');"><span>Add to Cart</span></button>
                                                </div>

                                            </div>


                                            <span class="related_default_width" style="display:none; visibility:hidden"></span>
                                        </div>

                                    </div>
                                    <div class="product-details">
                                        <div class="caption">
                                            <h4><a href="#">Wayang Palm Sugar 500g</a></h4>


                                            <p class="price">
                                                Size:30x500g
                                            </p>
                                            <p class="price">
                                                Product ID: 732324



                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript">
        <!--
        $('select[name=\'recurring_id\'], input[name="quantity"]').change(function() {
            $.ajax({
                url: 'index.php?route=product/product/getRecurringDescription',
                type: 'post',
                data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
                dataType: 'json',
                beforeSend: function() {
                    $('#recurring-description').html('');
                },
                success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();

                    if (json['success']) {
                        $('#recurring-description').html(json['success']);
                    }
                }
            });
        });
        //-->

    </script>
    <script type="text/javascript">
        <!--
        $('#button-cart').on('click', function() {
            $.ajax({
                url: 'index.php?route=checkout/cart/add',
                type: 'post',
                data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-cart').button('loading');
                },
                complete: function() {
                    $('#button-cart').button('reset');
                },
                success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json['error']) {
                        if (json['error']['option']) {
                            for (i in json['error']['option']) {
                                var element = $('#input-option' + i.replace('_', '-'));

                                if (element.parent().hasClass('input-group')) {
                                    element.parent().before('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                } else {
                                    element.before('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                }
                            }
                        }

                        if (json['error']['recurring']) {
                            $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    }

                    if (json['success']) {
                        $.notify({
                            message: json['success'],
                            target: '_blank'
                        }, {
                            // settings
                            element: 'body',
                            position: null,
                            type: "info",
                            allow_dismiss: true,
                            newest_on_top: false,
                            placement: {
                                from: "top",
                                align: "center"
                            },
                            offset: 0,
                            spacing: 10,
                            z_index: 2031,
                            delay: 5000,
                            timer: 1000,
                            url_target: '_blank',
                            mouse_over: null,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            onShow: null,
                            onShown: null,
                            onClose: null,
                            onClosed: null,
                            icon_type: 'class',
                            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
                                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&nbsp;&times;</button>' +
                                '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
                                '<div class="progress" data-notify="progressbar">' +
                                '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                                '</div>' +
                                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                                '</div>'
                        });

                        $('#cart > button').html('<i class="fa fa-shopping-cart"></i><span id="cart-total"> ' + json['total'] + '</span>');

                        //$('html, body').animate({ scrollTop: 0 }, 'slow');

                        $('#cart > ul').load('index1e1c.html?route=common/cart/info%20ul%20li');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
        //-->

    </script>
    <script type="text/javascript">
        <!--
        $('.date').datetimepicker({
            language: '',
            pickTime: false
        });

        $('.datetime').datetimepicker({
            language: '',
            pickDate: true,
            pickTime: true
        });

        $('.time').datetimepicker({
            language: '',
            pickDate: false
        });

        $('button[id^=\'button-upload\']').on('click', function() {
            var node = this;

            $('#form-upload').remove();

            $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

            $('#form-upload input[name=\'file\']').trigger('click');

            if (typeof timer != 'undefined') {
                clearInterval(timer);
            }

            timer = setInterval(function() {
                if ($('#form-upload input[name=\'file\']').val() != '') {
                    clearInterval(timer);

                    $.ajax({
                        url: 'index.php?route=tool/upload',
                        type: 'post',
                        dataType: 'json',
                        data: new FormData($('#form-upload')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                            $(node).button('loading');
                        },
                        complete: function() {
                            $(node).button('reset');
                        },
                        success: function(json) {
                            $('.text-danger').remove();

                            if (json['error']) {
                                $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                            }

                            if (json['success']) {
                                alert(json['success']);

                                $(node).parent().find('input').val(json['code']);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            }, 500);
        });
        //-->

    </script>
    <script type="text/javascript">
        <!--
        $('#review').delegate('.pagination a', 'click', function(e) {
            e.preventDefault();

            $('#review').fadeOut('slow');

            $('#review').load(this.href);

            $('#review').fadeIn('slow');
        });

        $('#review').load('index4a81.html?route=product/product/review&amp;product_id=41');

        $('#button-review').on('click', function() {
            $.ajax({
                url: 'index.php?route=product/product/write&product_id=41',
                type: 'post',
                dataType: 'json',
                data: $("#form-review").serialize(),
                beforeSend: function() {
                    $('#button-review').button('loading');
                },
                complete: function() {
                    $('#button-review').button('reset');
                },
                success: function(json) {
                    $('.alert-dismissible').remove();

                    if (json['error']) {
                        $('#review').after('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        $('#review').after('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                        $('input[name=\'name\']').val('');
                        $('textarea[name=\'text\']').val('');
                        $('input[name=\'rating\']:checked').prop('checked', false);
                    }
                }
            });
        });

        //$(document).ready(function() {
        //	$('.thumbnails').magnificPopup({
        //		type:'image',
        //		delegate: 'a',
        //		gallery: {
        //			enabled: true
        //		}
        //	});
        //});


        $(document).ready(function() {
            if ($(window).width() > 767) {
                $("#tmzoom").elevateZoom({

                    gallery: 'additional-carousel',
                    //inner zoom				 

                    zoomType: "inner",
                    cursor: "crosshair"

                    /*//tint
				
                    tint:true, 
                    tintColour:'#F90', 
                    tintOpacity:0.5
				
                    //lens zoom
				
                    zoomType : "lens", 
                    lensShape : "round", 
                    lensSize : 200 
				
                    //Mousewheel zoom
				
                    scrollZoom : true*/


                });
                var z_index = 0;

                $(document).on('click', '.thumbnail', function() {
                    $('.thumbnails').magnificPopup('open', z_index);
                    return false;
                });

                $('.additional-carousel a').click(function() {
                    var smallImage = $(this).attr('data-image');
                    var largeImage = $(this).attr('data-zoom-image');
                    var ez = $('#tmzoom').data('elevateZoom');
                    $('.thumbnail').attr('href', largeImage);
                    ez.swaptheimage(smallImage, largeImage);
                    z_index = $(this).index('.additional-carousel a');
                    return false;
                });

            } else {
                $(document).on('click', '.thumbnail', function() {
                    $('.thumbnails').magnificPopup('open', 0);
                    return false;
                });
            }
        });
        $(document).ready(function() {
            $('.thumbnails').magnificPopup({
                delegate: 'a.elevatezoom-gallery',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-with-zoom',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function(item) {
                        return item.el.attr('title');
                    }
                }
            });
        });


        //-->

    </script>
    <footer>
        <?php include_once'footer.php';?>
    </footer>
</body>

</html>
