
<!DOCTYPE html>
<html lang="en">

<head>
</head>

<body>
  <div class="container-scroller">
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <?php include_once 'top_header.php';?>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close mdi mdi-close"></i>
        <?php include_once 'right_sidebar.php';?>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <?php include_once 'sidebar_menu.php';?>
      </nav>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">View Banners</h4>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Sno</th>
                          <th>Title</th>
                          <th>Image</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $getBranches = getAllDataWithActiveRecent('banners'); $i=1;
                        if($getBranches->num_rows) {
                         ?>
                        <?php while ($row = $getBranches->fetch_assoc()) { ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $row['title']; ?></td>
                          <td><img src="<?php echo $base_url . './uploads/banner_image/'.$row['image'] ?>" width="50" height="50"></td> 
                          <td>
                              <a href="edit_banner.php?banner_id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit" data-original-title="Edit"><i class="mdi mdi-pencil fnt_ic_size"></i></a>

                              <?php if($row['is_active']==1){ ?>
                                <a onclick="return confirm('Confirm to Enable?');" href="enable.php?did=<?php echo $row['id']; ?>&dtable=<?php echo "banners"; ?>" data-toggle="tooltip" data-placement="bottom" title="Disable" data-original-title="Disable"><i class="mdi mdi-close fnt_ic_size"></i></a>
                              <?php } else { ?>
                                <a onclick="return confirm('Confirm to Disable?');" href="disable.php?did=<?php echo $row['id']; ?>&dtable=<?php echo "banners"; ?>" data-toggle="tooltip" data-placement="bottom" title="Enable" data-original-title="Enable"><i class="mdi mdi-check fnt_ic_size"></i></a>
                              <?php } ?>

                              <a href="#" data-toggle="modal" title="Image" data-target="#myModal<?php echo $i; ?>" data-placement="bottom" data-original-title="View"><i class="mdi mdi-eye fnt_ic_size"></i></a>
                              <div class="modal fade" id="myModal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel<?php echo $i; ?>">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">Banner Detail</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p><img src='<?php echo $base_url . './uploads/banner_image/'.$row['image'] ?>' width="350" height="350"></p>
                                        <table>
                                         <tr>
                                           <th>Title:</th>
                                           <td><?php echo $row['title']; ?></td>
                                         </tr>
                                         <tr>
                                           <th>Description:</th>
                                           <td><?php echo $row['description']; ?></td>
                                         </tr>
                                         <tr>
                                           <th>Banner About Us:</th>
                                           <td><?php echo $row['banner_about_us']; ?></td>
                                         </tr>
                                         <tr>
                                           <th>Meta Title:</th>
                                           <td><?php echo $row['meta_title']; ?></td>
                                         </tr>
                                         <tr>
                                           <th>Meta Keyword:</th>
                                           <td><?php echo $row['meta_keyword']; ?></td>
                                         </tr>
                                         <tr>
                                           <th>Meta Tag:</th>
                                           <td><?php echo $row['meta_tag']; ?></td>
                                         </tr>
                                         <tr>
                                           <th>Meta Description:</th>
                                           <td><?php echo $row['meta_description']; ?></td>
                                         </tr>
                                       </table>
                                    </div>

                                  </div><!-- /.modal-content -->
                                  </div><!-- /.modal-content -->
                              </div><!-- /.modal-content -->
                          </td>
                        </tr>
                        <?php $i++;  } }else{ ?>
                          <tr><td colspan="7">No Banners Found</td></tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    <div class="d-flex mt-4">
                      <!-- <nav class="ml-auto"><?php echo $links; ?></nav> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
           
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <?php include_once 'footer.php';?>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
</body>

</html>
