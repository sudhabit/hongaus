
<!DOCTYPE html>
<html lang="en">
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <?php include_once 'top_header.php';?>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close mdi mdi-close"></i>
        <?php include_once 'right_sidebar.php';?>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <?php include_once 'sidebar_menu.php';?>
      </nav>
      <?php  
        $product_id = $_GET['product_id'];
        //echo "<script>alert($cat_id);</script>"; exit;
      ?>
<?php  
 if (!isset($_POST['submit']))  {
    } else {

    $cat_id = $_REQUEST['cat_id'];
    $sub_cat_id = $_REQUEST['sub_cat_id'];
    $subsubcat_id = $_REQUEST['subsubcat_id'];
    $is_available = $_REQUEST['is_available'];
    $pack_size = $_REQUEST['pack_size'];
    $product_name = $_REQUEST['product_name'];
    $product_details = $_REQUEST['product_details'];
    $meta_title = $_REQUEST['meta_title'];
    $meta_keyword = $_REQUEST['meta_keyword'];
    $meta_tag = $_REQUEST['meta_tag'];
    $meta_description = $_REQUEST['meta_description'];

    $logo = $_FILES["logo"]["name"];
    $target_dir = "../uploads/product_images/";
    $target_file = $target_dir . basename($_FILES["logo"]["name"]);
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    if($_FILES["logo"]["name"]!='') {
        
            $logo = $_FILES["logo"]["name"];
            $target_dir = "../uploads/product_images/";
            $target_file = $target_dir . basename($_FILES["logo"]["name"]);
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        $getImgUnlink = getImageUnlink('image','products','id',$product_id,$target_dir);
        
        //Send parameters for img val,tablename,clause,id,imgpath for image ubnlink from folder
        if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
            $sql = "UPDATE `products` SET cat_id='$cat_id',sub_cat_id='$sub_cat_id',is_available='$is_available',pack_size='$pack_size',product_name = '$product_name',product_details = '$product_details', meta_title = '$meta_title',  meta_keyword = '$meta_keyword',meta_tag='$meta_tag', meta_description ='$meta_description',image='$logo', subsubcat_id='$subsubcat_id' WHERE id = '$product_id'";
                $result = queryExecute($sql);
                if( $result == 1){
                    echo "<script type='text/javascript'>window.location='edit_product.php?msg=succ'</script>";
                } else {
                    echo "<script type='text/javascript'>window.location='edit_product.php?msg=fail'</script>";
                }
            //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }

    }
     else {
        $sql = "UPDATE `products` SET cat_id='$cat_id',sub_cat_id='$sub_cat_id',is_available='$is_available',pack_size='$pack_size',product_name = '$product_name',product_details = '$product_details', meta_title = '$meta_title',  meta_keyword = '$meta_keyword',meta_tag='$meta_tag', meta_description ='$meta_description', subsubcat_id='$subsubcat_id' WHERE id = '$product_id'";
            $result = queryExecute($sql);
            if( $result == 1){
                echo "<script type='text/javascript'>window.location='edit_product.php?msg=succ'</script>";
            } else {
                echo "<script type='text/javascript'>window.location='edit_product.php?msg=fail'</script>";
            }
    }      
}
?>
      <!-- partial -->
      <?php $getData = getIndividualDetails('products','id',$product_id); ?>
      <div class="main-panel">        
        <div class="content-wrapper">
          <div class="row">
            
            <div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edit Product</h4>
                  <?php 
                    if($_GET['msg']=='succ'){
                    ?>
                        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <strong>Data Added Successfully</strong>
                        </div>
                    <?php
                    header( "refresh:2;url=view_products.php" );
                    } elseif($_GET['msg']=='fail'){
                    ?>
                        <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <strong>Oh!</strong> Your data adding failed.
                        </div>
                    <?php
                    }             
                    ?>
                  <form class="forms-sample" method="POST" enctype="multipart/form-data">
                  <div class="form-group row">
                       <label class="col-sm-3 col-form-label">Select Category</label>
                       <div class="col-sm-9">
                           <select class="js-example-basic-single" style="width: 100%" name="cat_id" id="cat_Id" onChange="getSubCategories(this.value);">
                             <option value="">Select Category </option>
                             <?php $getCategories = getAllDataWithStatus('categories','0');?>
                              <?php while($row = $getCategories->fetch_assoc()) {  ?>
                                  <option <?php if($row['id'] == $getData['cat_id']) { echo "Selected"; } ?> value="<?php echo $row['id']; ?>"><?php echo $row['category_name']; ?></option>
                              <?php } ?>
                           </select>
                        </div>
                    </div>
                    <div class="form-group row">
                       <label class="col-sm-3 col-form-label">Select Sub Category</label>
                       <div class="col-sm-9">
                           <select class="js-example-basic-single" style="width: 100%" name="sub_cat_id" id="subcatId">
                             <option value="">Select Sub Category</option>
                                        <?php $getSubCategories = getAllDataWithStatus('sub_categories','0');?>
                                        <?php while($row = $getSubCategories->fetch_assoc()) {  ?>
                                            <option <?php if($row['id'] == $getData['sub_cat_id']) { echo "Selected"; } ?> value="<?php echo $row['id']; ?>" ><?php echo $row['subcategory_name']; ?></option>
                                        <?php } ?>
                           </select>
                        </div>
                    </div> 
                     <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Select Sub sub category</label>
                        <div class="col-sm-9">
                            <select class="js-example-basic-single" style="width: 100%" name="subsubcat_id" id="subsubcatId">
                              <option value="">Select Sub subcategory </option>
                              <?php $getSubsubCategories = getAllDataWithStatus('sub_sub_categories','0');?>
                                        <?php while($row = $getSubsubCategories->fetch_assoc()) {  ?>
                                            <option <?php if($row['id'] == $getData['subsubcat_id']) { echo "Selected"; } ?> value="<?php echo $row['id']; ?>" ><?php echo $row['sub_sub_category_name']; ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group row">
                       <label class="col-sm-3 col-form-label">Is Available</label>
                       <div class="col-sm-9">
                           <select class="js-example-basic-single" style="width: 100%" name="is_available">
                             <option value="">Select Available </option>
                                  <option <?php if($getData['is_available'] == 0) { echo "Selected"; } ?> value="0" >Available</option>
                                  <option <?php if($getData['is_available'] == 1) { echo "Selected"; } ?> value="1" >Not Available</option>
                           </select>
                    </div>
                   </div> 
                   <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Pack Size</label>
                      <div class="col-sm-9">
                        <input type="text" name="pack_size" class="form-control" placeholder="Pack Size" value="<?php echo $getData['pack_size']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Product Name</label>
                      <div class="col-sm-9">
                        <input type="text" name="product_name" class="form-control" id="exampleInputUsername2" placeholder="Product Name" value="<?php echo $getData['product_name']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Image</label>
                      <div class="col-sm-9 grid-margin stretch-card">
                        <div class="card">
                          <div class="card-body">
                            <h4 class="card-title d-flex">Image
                            </h4>
                            <img src='<?php echo $base_url . './uploads/product_images/'.$getData['image'] ?>' height='250' width='250'>
                            <input type="file" name="logo"/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Product Details</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="product_details" rows="5"><?php echo $getData['product_details']; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Title</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_title" class="form-control" id="exampleInputUsername2" placeholder="Meta Title Name" value="<?php echo $getData['meta_title']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Keyword</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_keyword" class="form-control" id="exampleInputUsername2" placeholder="Meta Keyword" value="<?php echo $getData['meta_keyword']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Tag</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_tag" class="form-control" id="exampleInputUsername2" placeholder="Meta Tag" value="<?php echo $getData['meta_tag']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Description</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="meta_description" rows="5" ><?php echo $getData['meta_description']; ?></textarea>
                      </div>
                    </div>
                    <button type="submit" name="submit" value="submit" class="btn btn-primary mr-2">Submit</button>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
            <?php include_once 'footer.php';?>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
</body>

</html>
<script type="text/javascript">
function getSubCategories(val) {
      $.ajax({
      type: "POST",
      url: "get_sub_categories.php",
      data:'category_id='+val,
      success: function(data){
          $("#subcatId").html(data);
      }
      });
  }
  function getSubsubCategories(val) {
      $.ajax({
      type: "POST",
      url: "get_sub_sub_categories.php",
      data:'sub_category_id='+val,
      success: function(data){
          $("#subsubcatId").html(data);
      }
      });
  }
</script>

