<?php
session_start(); //to ensure you are using same session
//Update Log
include_once('../admin_includes/config.php');
include_once('../admin_includes/common_functions.php');

$admin_login_user_id = $_SESSION['admin_login_user_id'];
$last_logout = date("Y-m-d h:i:s");
$logOuttime = "UPDATE `admin_users` SET last_logout = '$last_logout' WHERE id = '$admin_login_user_id' ";
$conn->query($logOuttime);
session_unset();
session_destroy();
ob_start(); //destroy the session
header("location:index.php"); //to redirect back to "index.php" after logging out
exit();
?>