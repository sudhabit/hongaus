(function($) {
  'use strict';
  var verticalForm = $("#example-vertical-wizard");
  var siteUrl ="http://localhost/keyaan_new_admin/";
  verticalForm.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    stepsOrientation: "vertical",
    onStepChanging: function (event, currentIndex) {
      console.log(currentIndex);
      console.log(event);
      console.log("city value"+$(".city_id2").val());
      var stepIndex =  currentIndex ;
      var packageList =   $(".packageList").val();
      var budget_price =  $("#budget_price").val();
      var budget_amount = $("#budget_amount").val();
      var vendor_code =   $(".vendor_code").val();
      var member_ship =  $(".member_ship").val();
      var mobile =  $(".phoneNumber").val();
      var city =  $(".city").val();
      var cat_id = [];
      var city_id = [];
      $('.cat_id2 :selected').each(function(i, selected) {
        cat_id[i] = $(selected).val();
      });
      $('.city_id2 :selected').each(function(i, selected) {
        city_id[i] = $(selected).val();
      });
      var subcat = [];
      $(".subcat_vendorchk:checked").each(function() {
        subcat.push(this.value);
      });
      var subsubcat = [];
      $(".subsubcat_vendorchk:checked").each(function() {
        subsubcat.push(this.value);
      });
      if(stepIndex === 0 ){
       console.log("steps 1");
        $.ajax({
          url: siteUrl+"Vendor/stepOneVendorAll",
          type: 'POST',
          data: 'vendor_code=' + vendor_code + '&member_ship=' + member_ship + '&mobile=' + mobile + '&city=' + city + '&city_id=' + city_id + '&cat_id=' + cat_id,
          dataType: 'html',
          success: function(response) {
            // alert(response);
            $("#txtDataArea").val(response);
            $(".section_catList").html(response);
           // console.log(JSON.stringify(response));
            return false; 
            // if(response==1){
            //   return true;
            // }else{
            //   return false;
            // }
          },
          error: function(jqXHR, textStatus, errorThrown){
            console.log(textStatus, errorThrown);
          }
        });
      }
      else if(stepIndex === 1 ){
       // alert('step2')
      }
      if(stepIndex === 2){
          console.log("steps 3");
          $.ajax({
           url: siteUrl+"Vendor/stepThreeVendorAll",
           type: 'POST',
           data: 'vendor_code=' + vendor_code + '&member_ship=' + member_ship + '&mobile=' + mobile + '&city=' + city + '&city_id=' + city_id + '&cat_id=' + cat_id + '&subcat_id=' + subcat + '&packageList=' + packageList + '&budget_price=' + budget_price + '&budget_amount=' + budget_amount ,
           dataType: 'html',
           success: function(response) {
            // alert(response);
            //  $("#txtDataArea").val(response);
             $(".subsubcat_section").html(response);
             $('.js-example-basic-single').select2();
            //console.log(JSON.stringify(response));
            return false; 
            // if(response==1){
            //   return true;
            // }else{
            //   return false;
            // }
           },
           error: function(jqXHR, textStatus, errorThrown){
             console.log(textStatus, errorThrown);
           }
         });
       }
      else if(stepIndex === 3 ){
       // alert('step4');
        // $.ajax({
        //   url: siteUrl+"Vendor/stepTourVendorAll",
        //   type: 'POST',
        //   data: 'vendor_code=' + vendor_code ,
        //   dataType: 'html',
        //   success: function(response) {
        //  // alert(response);
        //   // console.log(JSON.stringify(response));
        //    return false; 
        //   },
        //   error: function(jqXHR, textStatus, errorThrown){
        //     console.log(textStatus, errorThrown);
        //   }
        // });
      }
      else if(stepIndex === 4 ){

        //alert('step5')
      }
      verticalForm.validate().settings.ignore = ":disabled,:hidden";
      return verticalForm.valid();
    },
    saveState: true,
    onFinished: function(event, currentIndex) {
      var packageList = $(".packageList").val();
      var budget_price = $("#budget_price").val();
      var budget_amount = $("#budget_amount").val();
      var vendor_code =  $(".vendor_code").val();
      var member_ship =  $(".member_ship").val();
      var mobile =  $(".phoneNumber").val();
      var pay_type =  $(".pay_type").val();
      var owner_name =  $(".owner_name").val();
      var company_name =  $(".company_name").val();
      var company_type =  $(".company_type").val();
      var pan =  $(".pan").val();
      //var website =  $(".website").val();
      var pan_type =  $(".pan_type").val();
      var pan_number =  $(".pan_number").val();
      var no_of_partners =  $(".no_of_partners").val();
      var charitable_trust =  $(".charitable_trust").val();
      var pan_firm =  $(".pan_firm").val();
      var cin_number =  $(".cin_number").val();
      var proprietor_num =  $(".proprietor_num").val();
      var proprietor_val =  $(".proprietor_val").val();
      var card_number =  $(".card_number").val();
      var business_type =  $(".line_business").val();
      var landline_num =  $(".landline").val();
      var designation =  $(".designation").val();
      var owner_num =  $(".own_mobnumber").val();
      var poc_num =  $(".poc_mobnumber").val();
      var poc_name =  $(".poc_name").val();
      var cst_city =  $(".cst_city").val();
      var locality =  $(".locality").val();
      var address =  $(".address").val();
      var pincode =  $(".pincode").val();
      var landmark1 =  $(".landmark1").val();
      var email =  $(".email").val();
      var act_email =  $(".act_email").val();
      var tan =  $(".tan").val();
      var landmark2 =  $(".landmark2").val();
      var email2 =  $(".email2").val();
      var business =  $(".business").val();
      var executive =  $(".executive").val();
      var city =  $(".city").val();
      var payment_id = $(".payment_id").val();
      var cash =  $(".cash").val();
      var date_of_payment =  $(".date_of_payment").val();
      var cash_tds =  $(".cash_tds").val();
      var bank_name =  $(".bank_name").val();
      var branch_name =  $(".branch_name").val();
      var account_number =  $(".account_number").val();
      var cheque_date =  $(".cheque_date").val();
      var cheque_number =  $(".cheque_number").val();
      var cheque_amount =  $(".cheque_amount").val();
      var cheque_issuer =  $(".cheque_issuer").val();
      var cheque_file =  $(".cheque_file").val();
      var cheque_tds =  $(".cheque_tds").val();
      var transaction_id =  $(".transaction_id").val();
      var order_id =  $(".order_id").val();
      var online_amount =  $(".online_amount").val();
      var online_date =  $(".online_date").val();
      var cat_id = [];
      var city_id = [];
      $('.cat_id2 :selected').each(function(i, selected) {
        cat_id[i] = $(selected).val();
      });
      $('.city_id2 :selected').each(function(i, selected) {
        city_id[i] = $(selected).val();
      });
      var subcat = [];
      $(".subcat_vendorchk:checked").each(function() {
        subcat.push(this.value);
      });
      var subsubcat = [];
      $(".subsubcat_vendorchk:checked").each(function() {
        subsubcat.push(this.value);
      });
      if(payment_id == 0) {
        alert("Choose Any Payment Method");
        return false;
      } else {
        url = siteUrl+"Vendor/stepVendorFinal";
      }
      $(".payment_type").on('click',function(){
        payment_type =$(this).attr('paymentType');
        //alert(payment_type);
        if(payment_type == 1) {
          $('.cheque,.online').empty();
          $('.cheque,.online').hide();
          $("#ModalLabel").text("Cash Mode Details");
        } else if(payment_type == 2) {
          $('.cash,.online').empty();
          $('.cash,.online').hide();
          $("#ModalLabel").text("Cheque Mode Details");
        } else if(payment_type == 3) {
          $('.cheque,.cash').empty();
          $('.cheque,.cash').hide();
          $("#ModalLabel").text("Online CC Mode Details");
        }
      });
      $.ajax({
        url: url,
        type: 'POST',
        data: 'vendor_code=' + vendor_code + '&member_ship=' + member_ship + '&mobile=' + mobile + '&city=' + city + '&city_id=' + city_id + '&cat_id=' + cat_id + '&subcat_id=' + subcat + '&subsubcat=' + subsubcat + '&packageList=' + packageList + '&budget_price=' + budget_price + '&budget_amount=' + budget_amount + '&pay_type=' + pay_type + '&owner_name=' + owner_name + '&company_name=' + company_name + '&company_type=' + company_type + '&pan=' + pan + '&pan_type=' + pan_type + '&pan_number=' + pan_number + '&no_of_partners=' + no_of_partners + '&charitable_trust=' + charitable_trust + '&pan_firm=' + pan_firm + '&cin_number=' + cin_number + '&proprietor_num=' + proprietor_num + '&proprietor_val=' + proprietor_val + '&card_number=' + card_number + '&business_type=' + business_type + '&landline_num=' + landline_num + '&designation=' + designation + '&owner_num=' + owner_num + '&poc_num=' + poc_num + '&poc_name=' + poc_name + '&cst_city=' + cst_city + '&locality=' + locality + '&address=' + address + '&pincode=' + pincode + '&landmark1=' + landmark1 + '&email=' + email + '&act_email=' + act_email + '&tan=' + tan + '&landmark2=' + landmark2 + '&email2=' + email2 + '&business=' + business + '&executive=' + executive + '&cash=' + cash + '&date_of_payment=' + date_of_payment + '&cash_tds=' + cash_tds + '&bank_name=' + bank_name + '&branch_name=' + branch_name + '&account_number=' + account_number + '&cheque_date=' + cheque_date + '&cheque_number=' + cheque_number + '&cheque_amount=' + cheque_amount + '&cheque_issuer=' + cheque_issuer + '&cheque_file=' + cheque_file + '&cheque_tds=' + cheque_tds + '&transaction_id=' + transaction_id + '&order_id=' + order_id + '&online_amount=' + online_amount + '&online_date=' + online_date ,
        dataType: 'html',
        success: function(response) {
          //alert(response);
          //  $("#txtDataArea").val(response);
            $(".subsubcat_section").html(response);
            $('.js-example-basic-single').select2();
          //console.log(JSON.stringify(response));
          var file_data = $('.website').prop('files')[0];  
          var form_data = new FormData();   
          form_data.append('vendor_code',vendor_code);               
          form_data.append('website', file_data);
          if(file_data) {
            $.ajax({
              url: siteUrl+"Vendor/vendorWebsite",
              type: "POST",
              data: form_data,
              processData: false, // important
              contentType: false, // important
              cache:false,
              async:true,
              success: function(data){
                  console.log(data);
              },
              // error: function(XMLHttpRequest, textStatus, errorThrown) { 
              //     alert("Status: " + textStatus); alert("Error: " + errorThrown); 
              // }  
            });
          }
          //For Pan Prrof
          var file_data1 = $('.pan_proof').prop('files')[0];  
          var form_data = new FormData();   
          form_data.append('vendor_code',vendor_code);               
          form_data.append('pan_proof', file_data1);
          if(file_data1) {
            $.ajax({
              url: siteUrl+"Vendor/vendorPanproof",
              type: "POST",
              data: form_data,
              processData: false, // important
              contentType: false, // important
              cache:false,
              async:true,
              success: function(data){
                  console.log(data);
              },
              // error: function(XMLHttpRequest, textStatus, errorThrown) { 
              //     alert("Status: " + textStatus); alert("Error: " + errorThrown); 
              // }  
            });
          }
          //For Pan Id
          var file_data2 = $('.pan_id').prop('files')[0];  
          var form_data = new FormData();   
          form_data.append('vendor_code',vendor_code);               
          form_data.append('pan_id', file_data2);
          if(file_data2) {
            $.ajax({
              url: siteUrl+"Vendor/vendorPanid",
              type: "POST",
              data: form_data,
              processData: false, // important
              contentType: false, // important
              cache:false,
              async:true,
              success: function(data){
                  console.log(data);
              },
              // error: function(XMLHttpRequest, textStatus, errorThrown) { 
              //     alert("Status: " + textStatus); alert("Error: " + errorThrown); 
              // }  
            });
          }
          //For Cin Id
          var file_data3 = $('.cin_id').prop('files')[0];  
          var form_data = new FormData();   
          form_data.append('vendor_code',vendor_code);               
          form_data.append('cin_id', file_data3);
          if(file_data3) {
            $.ajax({
              url: siteUrl+"Vendor/vendorCinid",
              type: "POST",
              data: form_data,
              processData: false, // important
              contentType: false, // important
              cache:false,
              async:true,
              success: function(data){
                  console.log(data);
              },
              // error: function(XMLHttpRequest, textStatus, errorThrown) { 
              //     alert("Status: " + textStatus); alert("Error: " + errorThrown); 
              // }  
            });
          }
          //For Id Proof
          var file_data4 = $('.id_proof').prop('files')[0];  
          var form_data = new FormData();   
          form_data.append('vendor_code',vendor_code);               
          form_data.append('id_proof', file_data4);
          if(file_data4) {
            $.ajax({
              url: siteUrl+"Vendor/vendorIdproof",
              type: "POST",
              data: form_data,
              processData: false, // important
              contentType: false, // important
              cache:false,
              async:true,
              success: function(data){
                  console.log(data);
              },
              // error: function(XMLHttpRequest, textStatus, errorThrown) { 
              //     alert("Status: " + textStatus); alert("Error: " + errorThrown); 
              // }  
            });
          }
          $(".updateSuss").show();
          setTimeout(function(){  location.reload(); }, 3000);
        },
        error: function(jqXHR, textStatus, errorThrown){
          console.log(textStatus, errorThrown);
        }
      });
     
      //alert("Submitted!");
     //console.log(currentIndex);
    }
  });
})(jQuery);