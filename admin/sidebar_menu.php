 <ul class="nav">
          <!-- <li class="nav-item">
            <a class="nav-link" href="dashboard.php">
              <i class="mdi mdi-view-quilt menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li> -->
         <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-settings" aria-expanded="false" aria-controls="ui-settings">
              <i class="mdi mdi-palette menu-icon"></i>
              <span class="menu-title">Settings</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-settings">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="basic_settings.php">Basic Settings</a></li>
                <li class="nav-item"> <a class="nav-link" href="add_social_settings.php">Social Media Settings</a></li>
              </ul>
            </div>
          </li>
          <!-- <li class="nav-item">
           <a class="nav-link" data-toggle="collapse" href="#ui-admin" aria-expanded="false" aria-controls="ui-admin">
             <i class="mdi mdi-palette menu-icon"></i>
             <span class="menu-title">Admin</span>
             <i class="menu-arrow"></i>
           </a>
           <div class="collapse" id="ui-admin">
             <ul class="nav flex-column sub-menu">
               <li class="nav-item"> <a class="nav-link" href="view_admin.php">View Admin</a></li>
               <li class="nav-item"> <a class="nav-link" href="add_admin.php">Add Admin</a></li>
             </ul>
           </div>
         </li> -->
       <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-banner" aria-expanded="false" aria-controls="ui-banner">
              <i class="mdi mdi-palette menu-icon"></i>
              <span class="menu-title">Banners</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-banner">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="view_banner.php">View Banner</a></li>
                <li class="nav-item"> <a class="nav-link" href="add_banner.php">Add Banner</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-category" aria-expanded="false" aria-controls="ui-category">
              <i class="mdi mdi-palette menu-icon"></i>
              <span class="menu-title">Category</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-category">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="view_category.php">View Category</a></li>
                <li class="nav-item"> <a class="nav-link" href="add_category.php">Add Category</a></li>
              </ul>
            </div>
          </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-subcategory" aria-expanded="false" aria-controls="ui-subcategory">
              <i class="mdi mdi-palette menu-icon"></i>
              <span class="menu-title">Sub Category</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-subcategory">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="view_subcategory.php">View Sub Category</a></li>
                <li class="nav-item"> <a class="nav-link" href="add_subcategory.php">Add Sub Category</a></li>
              </ul>
            </div>
          </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-sub_sub_category" aria-expanded="false" aria-controls="ui-sub_sub_category">
              <i class="mdi mdi-palette menu-icon"></i>
              <span class="menu-title">Sub Sub Category</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-sub_sub_category">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="view_sub_sub_category.php">View Sub Sub Category</a></li>
                <li class="nav-item"> <a class="nav-link" href="add_sub_sub_category.php">Add Sub Sub Category</a></li>
              </ul>
            </div>
          </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-products" aria-expanded="false" aria-controls="ui-products">
              <i class="mdi mdi-palette menu-icon"></i>
              <span class="menu-title">Products</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-products">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="view_products.php">View Products</a></li>
                <li class="nav-item"> <a class="nav-link" href="add_product.php">Add Products</a></li>
              </ul>
            </div>
          </li>
            <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-recipes" aria-expanded="false" aria-controls="ui-recipes">
              <i class="mdi mdi-palette menu-icon"></i>
              <span class="menu-title">Recipes</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-recipes">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="view_recipes.php">View Recipes</a></li>
                <li class="nav-item"> <a class="nav-link" href="add_recipes.php">Add Recipes</a></li>
              </ul>
            </div>
          </li>
        </ul>