<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>New</title>
        <!-- Vendor CSS -->
        <link href="assets/css1/animate.min.css" rel="stylesheet">
        <link href="assets/css1/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="assets/css1/app_1.min.css" rel="stylesheet">
        <link href="assets/css1/app_2.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="login-content">
         <!-- Login -->
         
        <div class="lc-block toggled" id="l-login">
            <div class="lcb-form">
                <img src="<?php echo 'http://localhost/new_project/'. './uploads/'.'logo-1.png' ?>">
                <form role="form" method="post" action="login-script.php" autocomplete = "off">
                    <div class="input-group m-b-20 m-t-20">
                        <span class="input-group-addon"><!-- <i class="zmdi zmdi-account"></i> --></span>
                        <div class="fg-line">
                            <input type="text" class="form-control"  name="admin_email" placeholder="Email" required>                        
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><!-- <i class="zmdi zmdi-male"></i> --></span>
                        <div class="fg-line">
                            <input type="password" class="form-control" placeholder="Password"  name="admin_password" required>                            
                        </div>
                    </div>
                    <input type="submit" name="submit" class="btn bgm-indigo waves-effect" style="margin-top: 20px;" value="Login">
                </form>
            </div>
        </div>
    </div>
        <!-- Javascript Libraries -->
        <script src="<?php echo base_url(); ?>assets1/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets1/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="<?php echo base_url(); ?>assets1/vendors/bower_components/Waves/dist/waves.min.js"></script>

        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url(); ?>assets1/js/app.min.js"></script>
    </body>

</html>
