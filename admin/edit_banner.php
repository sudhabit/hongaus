
<!DOCTYPE html>
<html lang="en">
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <?php include_once 'top_header.php';?>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close mdi mdi-close"></i>
        <?php include_once 'right_sidebar.php';?>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <?php include_once 'sidebar_menu.php';?>
      </nav>
      <?php  
        $banner_id = $_GET['banner_id'];
        //echo "<script>alert($cat_id);</script>"; exit;
      ?>
<?php  
 if (!isset($_POST['submit']))  {
    } else {

    $title = $_REQUEST['title'];
    $description = $_REQUEST['description'];

    $meta_title = $_REQUEST['meta_title'];
    $meta_keyword = $_REQUEST['meta_keyword'];
    $meta_tag = $_REQUEST['meta_tag'];
    $meta_description = $_REQUEST['meta_description'];
    
    if($_FILES["logo"]["name"]!='') {
        
            $logo = $_FILES["logo"]["name"];
            $target_dir = "../uploads/banner_image/";
            $target_file = $target_dir . basename($_FILES["logo"]["name"]);
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        $getImgUnlink = getImageUnlink('image','banners','id',$banner_id,$target_dir);
        
        //Send parameters for img val,tablename,clause,id,imgpath for image ubnlink from folder
        if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
            $sql = "UPDATE `banners` SET title = '$title',description = '$description', meta_title = '$meta_title',  meta_keyword = '$meta_keyword',meta_tag='$meta_tag', meta_description ='$meta_description', image = '$logo' WHERE id = '$banner_id' ";
                $result = queryExecute($sql);
                if( $result == 1){
                    echo "<script type='text/javascript'>window.location='edit_banner.php?msg=succ'</script>";
                } else {
                    echo "<script type='text/javascript'>window.location='edit_banner.php?msg=fail'</script>";
                }
            //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }

    }
     else {
        $sql = "UPDATE `banners` SET title = '$title',description = '$description',banner_about_us = '$banner_about_us', meta_title = '$meta_title',  meta_keyword = '$meta_keyword',meta_tag='$meta_tag', meta_description ='$meta_description' WHERE id = '$banner_id' ";
            $result = queryExecute($sql);
            if( $result == 1){
                echo "<script type='text/javascript'>window.location='edit_banner.php?msg=succ'</script>";
            } else {
                echo "<script type='text/javascript'>window.location='edit_banner.php?msg=fail'</script>";
            }
    }   
    
}
?>
      <!-- partial -->
      <?php $row = getIndividualDetails('banners','id',$banner_id); ?>
      <div class="main-panel">        
        <div class="content-wrapper">
          <div class="row">
            
            <div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edit Banner</h4>
                  <?php 
                    if($_GET['msg']=='succ'){
                    ?>
                        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <strong>Data Added Successfully</strong>
                        </div>
                    <?php
                    header( "refresh:2;url=view_banner.php" );
                    } elseif($_GET['msg']=='fail'){
                    ?>
                        <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <strong>Oh!</strong> Your data adding failed.
                        </div>
                    <?php
                    }             
                    ?>
                  <form class="forms-sample" method="POST" enctype="multipart/form-data">  
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Title</label>
                      <div class="col-sm-9">
                        <input type="text" name="title" class="form-control" id="exampleInputUsername2" placeholder="Title" value="<?php echo $row['title']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Image</label>
                      <div class="col-sm-9 grid-margin stretch-card">
                        <div class="card">
                          <div class="card-body">
                            <h4 class="card-title d-flex">Image
                            </h4>
                            <img src='<?php echo $base_url . './uploads/banner_image/'.$row['image'] ?>'>
                            <input type="file" name="logo"/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Description</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="Description" id="exampleInputEmail2" rows="5"><?php echo $row['description']; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Title</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_title" class="form-control" id="exampleInputUsername2" placeholder="Meta Title Name" value="<?php echo $row['meta_title']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Keyword</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_keyword" class="form-control" id="exampleInputUsername2" placeholder="Meta Keyword" value="<?php echo $row['meta_keyword']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Tag</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_tag" class="form-control" id="exampleInputUsername2" placeholder="Meta Tag" value="<?php echo $row['meta_tag']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Description</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="meta_description" rows="5" ><?php echo $row['meta_description']; ?></textarea>
                      </div>
                    </div>
                    <button type="submit" name="submit" value="submit" class="btn btn-primary mr-2">Submit</button>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
            <?php include_once 'footer.php';?>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
</body>

</html>
