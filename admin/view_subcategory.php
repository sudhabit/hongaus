
<!DOCTYPE html>
<html lang="en">

<head>
</head>

<body>
  <div class="container-scroller">
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <?php include_once 'top_header.php';?>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close mdi mdi-close"></i>
        <?php include_once 'right_sidebar.php';?>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <?php include_once 'sidebar_menu.php';?>
      </nav>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">View Sub Categories</h4>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Sno</th>
                          <th>Category Name</th>
                          <th>Sub Category</th>
                          <th>Image</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $getBranches = getAllDataWithActiveRecent('sub_categories'); $i=1;
                        if($getBranches->num_rows) {
                         ?>
                        <?php while ($row = $getBranches->fetch_assoc()) { ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <!-- <td><?php echo $getCategoryName['cat_id']; ?></td> -->
                          <td><?php $getCategories = getAllData('categories'); while($getCategoriesData = $getCategories->fetch_assoc()) { if($row['cat_id'] == $getCategoriesData['id']) { echo $getCategoriesData['category_name']; } } ?></td>
                          <td><?php echo $row['subcategory_name']; ?></td>
                          <td><img src="<?php echo $base_url . './uploads/sub_category_image/'.$row['image'] ?>" width="50" height="50"></td> 
                          <td>
                              <a href="edit_subcategory.php?subcat_id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit" data-original-title="Edit"><i class="mdi mdi-pencil fnt_ic_size"></i></a>
                              <?php if($row['is_active']==1){ ?>
                                <a onclick="return confirm('Confirm to Enable?');" href="enable.php?did=<?php echo $row['id']; ?>&dtable=<?php echo "sub_categories"; ?>" data-toggle="tooltip" data-placement="bottom" title="Disable" data-original-title="Disable"><i class="mdi mdi-close fnt_ic_size"></i></a>
                              <?php } else { ?>
                                <a onclick="return confirm('Confirm to Disable?');" href="disable.php?did=<?php echo $row['id']; ?>&dtable=<?php echo "sub_categories"; ?>" data-toggle="tooltip" data-placement="bottom" title="Enable" data-original-title="Enable"><i class="mdi mdi-check fnt_ic_size"></i></a>
                              <?php } ?>
                              <a href="#" data-toggle="modal" title="Image" data-target="#myModal<?php echo $i; ?>" data-placement="bottom" data-original-title="View"><i class="mdi mdi-eye fnt_ic_size"></i></a>
                              <div class="modal fade" id="myModal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel<?php echo $i; ?>">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">SubCategory Detail</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p><img src='<?php echo $base_url . './uploads/sub_category_image/'.$row['image'] ?>' width="350" height="350"></p>
                                        <table>
                                        <tr>
                                          <th>SubCategory Name:</th>
                                          <td><?php echo $row['subcategory_name']; ?></td>
                                        </tr>
                                        </table>
                                    </div>

                                  </div><!-- /.modal-content -->
                                  </div><!-- /.modal-content -->
                              </div><!-- /.modal-content -->
                          </td>
                        </tr>
                        <?php $i++;  } }else{ ?>
                          <tr><td colspan="7">No Sub Category Found</td></tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    <div class="d-flex mt-4">
                      <!-- <nav class="ml-auto"><?php echo $links; ?></nav> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
           
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <?php include_once 'footer.php';?>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
</body>

</html>
