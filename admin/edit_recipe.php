
<!DOCTYPE html>
<html lang="en">
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <?php include_once 'top_header.php';?>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close mdi mdi-close"></i>
        <?php include_once 'right_sidebar.php';?>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <?php include_once 'sidebar_menu.php';?>
      </nav>
      <?php  
        $cat_id = $_GET['cat_id'];
        //echo "<script>alert($cat_id);</script>"; exit;
      ?>
<?php  
 if (!isset($_POST['submit']))  {
    } else {

    $category_name = $_REQUEST['category_name'];
    $category_details = $_REQUEST['category_details'];
    $meta_title = $_REQUEST['meta_title'];
    $meta_keyword = $_REQUEST['meta_keyword'];
    $meta_tag = $_REQUEST['meta_tag'];
    $meta_description = $_REQUEST['meta_description'];
    
    if($_FILES["logo"]["name"]!='') {
        
            $logo = $_FILES["logo"]["name"];
            $target_dir = "../uploads/recipe_image/";
            $target_file = $target_dir . basename($_FILES["logo"]["name"]);
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        $getImgUnlink = getImageUnlink('image','categories','id',$id,$target_dir);
        
        //Send parameters for img val,tablename,clause,id,imgpath for image ubnlink from folder
        if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
            $sql = "UPDATE `categories` SET category_name = '$category_name', meta_title = '$meta_title',  meta_keyword = '$meta_keyword',meta_tag='$meta_tag', meta_description ='$meta_description', image = '$logo',category_details ='$category_details' WHERE id = '$cat_id' ";
                $result = queryExecute($sql);
                if( $result == 1){
                    echo "<script type='text/javascript'>window.location='edit_category.php?msg=succ'</script>";
                } else {
                    echo "<script type='text/javascript'>window.location='edit_category.php?msg=fail'</script>";
                }
            //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }

    }
     else {
        $sql = "UPDATE `categories` SET category_name = '$category_name', meta_title = '$meta_title',  meta_keyword = '$meta_keyword',meta_tag='$meta_tag', meta_description ='$meta_description',category_details = '$category_details' WHERE id = '$cat_id' ";
            $result = queryExecute($sql);
            if( $result == 1){
                echo "<script type='text/javascript'>window.location='edit_category.php?msg=succ'</script>";
            } else {
                echo "<script type='text/javascript'>window.location='edit_category.php?msg=fail'</script>";
            }
    }   
    
}
?>
      <!-- partial -->
      <?php $row = getIndividualDetails('categories','id',$cat_id); ?>
      <div class="main-panel">        
        <div class="content-wrapper">
          <div class="row">
            
            <div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edit Recipes</h4>
                  <?php 
                    if($_GET['msg']=='succ'){
                    ?>
                        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <strong>Data Updated Successfully</strong>
                        </div>
                    <?php
                    header( "refresh:2;url=view_category.php" );
                    } elseif($_GET['msg']=='fail'){
                    ?>
                        <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <strong>Oh!</strong> Your data updation failed.
                        </div>
                    <?php
                    }             
                    ?>
                  <form class="forms-sample" method="POST" enctype="multipart/form-data">  
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Recipe Name</label>
                      <div class="col-sm-9">
                        <input type="text" name="recipe_name" class="form-control" id="exampleInputUsername2" placeholder="Recipe Name" value="<?php echo $row['recipe_name']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Image</label>
                      <div class="col-sm-9 grid-margin stretch-card">
                        <div class="card">
                          <div class="card-body">
                            <h4 class="card-title d-flex">Image
                            </h4>
                            <img src='<?php echo $base_url . './uploads/recipe_image/'.$row['image'] ?>'>
                            <input type="file" name="logo"/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Details</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="category_details" rows="5" ><?php echo $row['category_details']; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Title</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_title" class="form-control" id="exampleInputUsername2" placeholder="Meta Title Name" value="<?php echo $row['meta_title']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Keyword</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_keyword" class="form-control" id="exampleInputUsername2" placeholder="Meta Keyword" value="<?php echo $row['meta_keyword']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Tag</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_tag" class="form-control" id="exampleInputUsername2" placeholder="Meta Tag" value="<?php echo $row['meta_tag']; ?>" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Description</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="meta_description" rows="5" ><?php echo $row['meta_description']; ?></textarea>
                      </div>
                    </div>
                    <button type="submit" name="submit" value="submit" class="btn btn-primary mr-2">Submit</button>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
            <?php include_once 'footer.php';?>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
</body>

</html>
