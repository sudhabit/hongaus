
<!DOCTYPE html>
<html lang="en">

<head>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <?php include_once 'top_header.php';?>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close mdi mdi-close"></i>
        <?php include_once 'right_sidebar.php';?>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <?php include_once 'sidebar_menu.php';?>
      </nav>
      <!-- partial -->
            <?php  
 if (!isset($_POST['submit']))  {
    } else {

    $id=1;
    $site_name = $_POST['site_name'];  
    $site_email = $_POST['site_email']; 
    $site_mobile = $_POST['site_mobile']; 
    $site_landline = $_POST['site_landline'];
    $site_address  = $_POST['site_address'];
    $meta_title = $_POST['meta_title'];
    $meta_keywords = $_POST['meta_keywords'];
    $meta_tags = $_POST['meta_tags'];
    $meta_description = $_POST['meta_description'];
    $site_footertext = $_POST['site_footertext'];
    
    if($_FILES["logo"]["name"]!='') {
        
        $logo = $_FILES["logo"]["name"];
        $target_dir = "../uploads/site_logo/";
        $target_file = $target_dir . basename($_FILES["logo"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" && $imageFileType1 != "jpg" && $imageFileType1 != "png" && $imageFileType1 != "jpeg"
        && $imageFileType1 != "gif") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        $getImgUnlink = getImageUnlink('site_logo','basic_settings','id',$id,$target_dir);
        
        //Send parameters for img val,tablename,clause,id,imgpath for image ubnlink from folder
        if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
            $sql = "UPDATE `basic_settings` SET site_name = '$site_name', site_email = '$site_email',  site_mobile = '$site_mobile',site_landline='$site_landline', site_address ='$site_address', site_logo = '$logo', meta_title='$meta_title',meta_keywords = '$meta_keywords', meta_tags = '$meta_tags',  meta_description = '$meta_description',site_footertext='$site_footertext' WHERE id = 1 ";

            if($conn->query($sql) === TRUE){
           echo "<script type='text/javascript'>window.location='basic_settings.php?msg=success'</script>";
        } else {
           echo "<script type='text/javascript'>window.location='basic_settings.php?msg=fail'</script>";
        }
            //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }

    }
     else {
        $sql = "UPDATE `basic_settings` SET site_name = '$site_name', site_email = '$site_email',  site_mobile = '$site_mobile',site_landline='$site_landline', site_address ='$site_address', meta_title='$meta_title',meta_keywords = '$meta_keywords', meta_tags = '$meta_tags',  meta_description = '$meta_description',site_footertext='$site_footertext' WHERE id = '$id' ";
        if($conn->query($sql) === TRUE){
           echo "<script type='text/javascript'>window.location='basic_settings.php?msg=success'</script>";
        } else {
           echo "<script type='text/javascript'>window.location='basic_settings.php?msg=fail'</script>";
        }
    }   
    
}
?>
      <div class="main-panel">        
        <div class="content-wrapper">
          <div class="row">
            <?php $getData = getIndividualDetails('basic_settings','id',1); ?>
            <div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Basic Settings</h4>
                  <form class="forms-sample" method="POST" enctype="multipart/form-data">  
                    <div class="form-group row">
                      <label for="sitename" class="col-sm-3 col-form-label">Site Name</label>
                      <div class="col-sm-9">
                        <input type="text" name="site_name" class="form-control input-sm" placeholder="Website Name" value="<?php echo $getData['site_name']?>"  required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="sitemail" class="col-sm-3 col-form-label">Site Mail</label>
                      <div class="col-sm-9">
                        <input type="email" name="site_email" class="form-control input-sm" placeholder="Contact Email" value="<?php echo $getData['site_email']?>">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="sitemobile" class="col-sm-3 col-form-label">Site Mobile</label>
                      <div class="col-sm-9">
                        <input type="text" name="site_mobile"  class="form-control input-sm" placeholder="Contact Mobile" value="<?php echo $getData['site_mobile']; ?>"  required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="sitelandline" class="col-sm-3 col-form-label">Site Landline</label>
                      <div class="col-sm-9">
                        <input type="text" name="site_landline" class="form-control input-sm" placeholder="Contact Number" value="<?php echo $getData['site_landline']; ?>"  required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="siteaddress" class="col-sm-3 col-form-label">Site Address</label>
                      <div class="col-sm-9">
                        <textarea rows="5" name="site_address" class="form-control input-sm" placeholder="Site Address"><?php echo $getData['site_address']; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="sitename" class="col-sm-3 col-form-label">Meta Title</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_title" class="form-control input-sm" placeholder="Meta Title" value="<?php echo $getData['meta_title']?>"  required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="sitename" class="col-sm-3 col-form-label">Meta Keywords</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_keywords" class="form-control input-sm" placeholder="Meta Keywords" value="<?php echo $getData['meta_keywords']?>"  required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="sitename" class="col-sm-3 col-form-label">Meta Tags</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_tags" class="form-control input-sm" placeholder="Meta Tags" value="<?php echo $getData['meta_tags']?>"  required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="siteaddress" class="col-sm-3 col-form-label">Meta Description</label>
                      <div class="col-sm-9">
                        <textarea rows="5" name="meta_description" class="form-control input-sm" placeholder="Meta Description"><?php echo $getData['meta_description']; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="siteaddress" class="col-sm-3 col-form-label">Site Footer Text</label>
                      <div class="col-sm-9">
                        <textarea rows="5" name="site_footertext" class="form-control input-sm" placeholder="Site Footer Text"><?php echo $getData['site_footertext']; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Site Logo</label>
                      <div class="col-sm-9 grid-margin stretch-card">
                        <div class="card">
                          <div class="card-body">
                            <h4 class="card-title d-flex">Site Logo
                            </h4>
                            <img src='<?php echo $base_url . './uploads/site_logo/'.$getData['site_logo'] ?>'>
                            <input type="file" class="dropify" name="logo"/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $result[0]['id']; ?>">
                    <button type="submit" name="submit" class="btn btn-primary mr-2">Submit</button>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
            <?php include_once 'footer.php';?>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
</body>

</html>
