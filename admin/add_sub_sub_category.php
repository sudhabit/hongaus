
<!DOCTYPE html>
<html lang="en">
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <?php include_once 'top_header.php';?>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close mdi mdi-close"></i>
        <?php include_once 'right_sidebar.php';?>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <?php include_once 'sidebar_menu.php';?>
      </nav>
      <?php
        if (!isset($_POST['submit']))  {
          // echo "fail";
        } else  { 

            //echo "<pre>"; print_r($_POST); die;
            $cat_id = $_REQUEST['cat_id'];
            $sub_cat_id = $_REQUEST['sub_cat_id'];
            $sub_sub_category_name = $_REQUEST['sub_sub_category_name'];
            $sub_sub_category_details = $_REQUEST['sub_sub_category_details'];
            $meta_title = $_REQUEST['meta_title'];
            $meta_keyword = $_REQUEST['meta_keyword'];
            $meta_tag = $_REQUEST['meta_tag'];
            $meta_description = $_REQUEST['meta_description'];
            
            $image = $_FILES["image"]["name"];
            $target_dir = "../uploads/sub_sub_category_images/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                $sql = "INSERT INTO sub_sub_categories (`cat_id`,`sub_cat_id`,`sub_sub_category_name`,`image`,`sub_sub_category_details`,`meta_title`,`meta_keyword`,`meta_tag`,`meta_description`) VALUES ('$cat_id','$sub_cat_id','$sub_sub_category_name','$image','$sub_sub_category_details','$meta_title','$meta_keyword','$meta_tag','$meta_description')";

                $result = queryExecute($sql);
                if( $result == 1){
                    echo "<script type='text/javascript'>window.location='add_sub_sub_category.php?msg=succ'</script>";
                } else {
                    echo "<script type='text/javascript'>window.location='add_sub_sub_category.php?msg=fail'</script>";
                }
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
        ?> 
      <!-- partial -->
      <div class="main-panel">        
        <div class="content-wrapper">
          <div class="row">
            
            <div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add Sub Sub Category</h4>
                  <?php
                    if($_GET['msg']=='succ'){
                    ?>
                        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <strong>Data Added Successfully</strong>
                        </div>
                    <?php
                    header( "refresh:2;url=view_sub_sub_category.php" );
                    } elseif($_GET['msg']=='fail'){
                    ?>
                        <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <strong>Oh!</strong> Your data adding failed.
                        </div>
                    <?php
                    }             
                    ?>
                  <form class="forms-sample" method="POST" enctype="multipart/form-data">
                  <div class="form-group row">
                       <label class="col-sm-3 col-form-label">Select Category</label>
                       <div class="col-sm-9">
                           <select class="js-example-basic-single" style="width: 100%" name="cat_id" id="cat_Id" onChange="getSubCategories(this.value);">
                             <option value="">Select Category </option>
                             <?php $getCategories = getAllDataWithStatus('categories','0');?>
                              <?php while($row = $getCategories->fetch_assoc()) {  ?>
                                  <option value="<?php echo $row['id']; ?>" ><?php echo $row['category_name']; ?></option>
                              <?php } ?>
                           </select>
                    </div>
                   </div> 
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Select Sub category</label>
                        <div class="col-sm-9">
                            <select class="js-example-basic-single" style="width: 100%" name="sub_cat_id" id="subcatId">
                              <option value="">Select Subcategory </option>
                            </select>
                        </div>
                    </div>  
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Sub Sub Category Name</label>
                      <div class="col-sm-9">
                        <input type="text" name="sub_sub_category_name" class="form-control" id="exampleInputUsername2" placeholder="Sub Category Name" required>
                      </div>
                    </div>
                     <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Details</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="sub_sub_category_details" rows="5"></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Image</label>
                      <div class="col-sm-9 grid-margin stretch-card">
                        <div class="card">
                          <div class="card-body">
                            <h4 class="card-title d-flex">Image
                            </h4>
                            <input type="file" class="dropify" name="image" required/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Title</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_title" class="form-control" id="exampleInputUsername2" placeholder="Meta Title Name" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Keyword</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_keyword" class="form-control" id="exampleInputUsername2" placeholder="Meta Keyword" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Tag</label>
                      <div class="col-sm-9">
                        <input type="text" name="meta_tag" class="form-control" id="exampleInputUsername2" placeholder="Meta Tag" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Meta Description</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="meta_description" id="exampleInputEmail2" rows="5"></textarea>
                      </div>
                    </div>
                    <button type="submit" name="submit" value="submit" class="btn btn-primary mr-2">Submit</button>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
            <?php include_once 'footer.php';?>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
</body>

</html>
<script type="text/javascript">
function getSubCategories(val) {
      $.ajax({
      type: "POST",
      url: "get_sub_categories.php",
      data:'category_id='+val,
      success: function(data){
          $("#subcatId").html(data);
      }
      });
  }
</script>

