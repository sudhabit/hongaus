
<!DOCTYPE html>
<html lang="en">

<head>
</head>

<body>
  <div class="container-scroller">
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <?php include_once 'top_header.php';?>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close mdi mdi-close"></i>
        <?php include_once 'right_sidebar.php';?>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <?php include_once 'sidebar_menu.php';?>
      </nav>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">View Admin</h4>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Sno</th>
                          <th>Admin Name</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $getBranches = getAllDataWithActiveRecent('admin_users'); $i=1;
                        if($getBranches->num_rows) {
                         ?>
                        <?php while ($row = $getBranches->fetch_assoc()) { ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $row['admin_name']; ?></td>
                          
                          <td>
                              <a href="edit_admin.php?editadmin_id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit" data-original-title="Edit"><i class="mdi mdi-pencil fnt_ic_size"></i></a>
                              <?php if($row['lkp_status_id']==1){ ?>
                                <a onclick="return confirm('Confirm to Enable?');" href="enable.php?did=<?php echo $row['id']; ?>&dtable=<?php echo "admin_users"; ?>" data-toggle="tooltip" data-placement="bottom" title="Disable" data-original-title="Disable"><i class="mdi mdi-close fnt_ic_size"></i></a>
                              <?php } else { ?>
                                <a onclick="return confirm('Confirm to Disable?');" href="disable.php?did=<?php echo $row['id']; ?>&dtable=<?php echo "admin_users"; ?>" data-toggle="tooltip" data-placement="bottom" title="Enable" data-original-title="Enable"><i class="mdi mdi-check fnt_ic_size"></i></a>
                              <?php } ?>
                          </td>
                        </tr>
                        <?php $i++;  } }else{ ?>
                          <tr><td colspan="7">No Admin Users Found</td></tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    <div class="d-flex mt-4">
                      <!-- <nav class="ml-auto"><?php echo $links; ?></nav> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
           
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <?php include_once 'footer.php';?>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
</body>

</html>
