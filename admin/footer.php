<div class="d-sm-flex justify-content-center justify-content-sm-between">
  <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2018 Keyaan. All rights reserved.</span>
  <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
</div>
<!-- plugins:js -->
<script src="assets/vendors/js/vendor.bundle.base.js"></script>
<script src="assets/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="assets/js/off-canvas.js"></script>
<script src="assets/js/hoverable-collapse.js"></script>
<script src="assets/js/template.js"></script>
<script src="assets/js/settings.js"></script>
<script src="assets/js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="assets/js/file-upload.js"></script>

<script src="assets/js/typeahead.js"></script>
<script src="assets/js/dashboard.js"></script>
<script src="assets/js/allscript_ajax.js"></script>
<script src="assets/js/allscript.js"></script>

<script src="assets/js/dropify.js"></script>
<script src="assets/js/select2.js"></script>

<!-- End custom js for this page-->

<!-- Below script for ck editor -->
    <script src="//cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'description' );
    </script>
    
    <style type="text/css">
        .cke_top, .cke_contents, .cke_bottom {
            border: 1px solid #333;
        }
    </style>
  </body>
  <style>
    .modal-body{
      font-size:15px;
      text-align:justify;
      padding-left:110px;
      padding-top:30px;
      font-family:Roboto,sans-serif;
    }
    .open_cursor {
      cursor: pointer !important;
    }
  </style>