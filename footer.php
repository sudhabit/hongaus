 <div class="footerbefore">
            <div class="container">
                <script>
                    function subscribe() {
                        var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                        var email = $('#txtemail').val();
                        if (email != "") {
                            if (!emailpattern.test(email)) {
                                $('.text-danger').remove();
                                var str = '<span class="error">Invalid Email</span>';
                                $('#txtemail').after('<div class="text-danger">Invalid Email</div>');

                                return false;
                            } else {
                                $.ajax({
                                    url: 'index.php?route=extension/module/newsletters/news',
                                    type: 'post',
                                    data: 'email=' + $('#txtemail').val(),
                                    dataType: 'json',


                                    success: function(json) {

                                        $('.text-danger').remove();
                                        $('#txtemail').after('<div class="text-danger">' + json.message + '</div>');

                                    }

                                });
                                return false;
                            }
                        } else {
                            $('.text-danger').remove();
                            $('#txtemail').after('<div class="text-danger">Email Is Require</div>');
                            $(email).focus();

                            return false;
                        }
                    }

                </script>

                <div class="newsletter">


                    <h1>Get the Latest News Delivered Daily.</h1>
                    <p>Give us your email and you will be daily updated with the latest events, in detail. </p>
                    <div class="newsletter-message">


                    </div>


                    <div class="newsright">
                        <form action="#" method="post">
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-firstname">Email</label>
                                <div class="input-news">
                                    <input type="email" name="txtemail" id="txtemail" value="" placeholder="Enter Your Email Address" class="form-control input-lg" />
                                </div>
                                <div class="subscribe-btn">
                                    <button type="submit" class="btn btn-default btn-lg" onclick="return subscribe();">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <div id="footer" class="container">
            <div class="row">
                <div class="footer-recipe">
                    <a href="#">About</a>
                    <a href="#">Recipes</a>
                    <a href="#">Events & Promo</a>
                    <a href="#">Contact Us</a>
                </div>
                <div class="footer-recipe">
                    <a href="#">Dried Food</a>
                    <a href="#">Chilled Food</a>
                    <a href="#">Frozen Food</a>
                    <a href="#">Paper & Packing</a>
                    <a href="#">Cleaning Products</a>
                    <a href="#">Miscellaneous</a>
                </div>
            </div>
            <div class="row">
                <div class="footer-blocks">


                    <div id="info" class="col-sm-3 column">
                        <h5>Address</h5>
                        <ul class="list-unstyled">
                            <li><a href="#">150-156 McCredie Road,</a></li>
                            <li><a href="#">Smithfield,NSW,2164</a></li>
                            <li><a href="#">Australia</a></li>

                        </ul>
                    </div>

                    <div id="extra-link" class="col-sm-3 column">
                        <h5>Phone</h5>
                        <ul class="list-unstyled">
                            <li><a href="#">(612)8717-9999</a></li>

                        </ul>
                    </div>


                    <div class="col-sm-3 column">
                        <h5>Email</h5>
                        <ul class="list-unstyled">
                            <li><a href="#">info.syd@hongaustralia.com.au</a></li>

                        </ul>
                    </div>

                    <div class="col-sm-3 column footerright">
                        <div id="czfootercmsblock" class="footer-cms-block">
                            <h5>Hours</h5>
                            <ul>
                                <li>

                                    <span>7days a week from 9:00 am to 7:00 pm</span>
                                </li>

                            </ul>

                        </div>
                        <div class="block-social">
                            <ul>
                                <li class="facebook"><a href="#"><span>Facebook</span></a></li>
                                <li class="twitter"><a href="#"><span>Twitter</span></a></li>
                                <li class="youtube"><a href="#"><span>YouTube</span></a></li>
                                <li class="googleplus"><a href="#"><span>Google +</span></a></li>
                                <li class="pinterest"><a href="#"><span>Pinterest</span></a></li>
                            </ul>
                        </div>

                    </div>


                </div>
            </div>
        </div>

        <div id="bottom-footer" class="bottomfooter">
            <div class="container">
                <div class="row">

                    <p id="powered" class="powered">Copyrights 2016 Hong Australia Corporation Pvt Ltd. All Rights Reserved. <a href="#">INFY SMART</a> </p>



                </div>
            </div>
        </div>